import pyhog as d
from scipy.misc import imread
from datetime import datetime as dt

modelFileName = "head_W24x24_C4x4_N2x2_G4x4_HeadSize16x16.alt"


img = []
nFrames = 5
for iImg in range(nFrames):
    img.append(imread('../data/src/frame{:04d}.jpg'.format(iImg)))

st = dt.now()
print("Independent search:")
for iImg in range(nFrames):
    bboxArray = d.detect_head(img[iImg], modelFileName)
    print(bboxArray.shape)
print("Finished in " + str(dt.now() - st))

st = dt.now()
print("Independent asynchronous search:")
for iImg in range(5):
    width = img[iImg].shape[1]
    height = img[iImg].shape[0]
    d.init(width, height, modelFileName)
    d.start(img[iImg])
    bboxArray = d.stop()
    print(bboxArray.shape)
print("Finished in " + str(dt.now() - st))

st = dt.now()
print("Asynchronous search in a pipeline (check that all images have the same size!):")
d.init(width, height, modelFileName)
d.start(img[0])
for iImg in range(1, nFrames):
    bboxArray = d.next(img[iImg])
    print(bboxArray.shape)
bboxArray = d.stop()
print(bboxArray.shape)
print("Finished in " + str(dt.now() - st))
