#pragma once
struct ScaleParameters
{
  /// the current scale
  float scale;

  /// the number of columns in the resized image
  int width;
  /// the number of rows in the resized image
  int height;

  /// the number of blocks along X direction on the current scale
  int num_blocks_x;
  /// the number of blocks along Y direction on the current scale
  int num_blocks_y;

  /// the number of windows along X direction on the current scale
  int num_windows_x;
  /// the number of windows along Y direction on the current scale
  int num_windows_y;
};

