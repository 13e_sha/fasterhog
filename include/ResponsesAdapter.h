#pragma once

#include <vector>
#include <driver_types.h>

using std::vector;

class ResponsesAdapter
{
public:
  /// \brief
  /// Default constructor
  ResponsesAdapter();

  /// \brief
  /// Destructor
  ~ResponsesAdapter();

  /// \brief
  /// Returns responses on the current level of pyramid
  ///
  /// \param[in] in_level The index of the level in the pyramid
  /// \return             The pointer to the responses on the current level
  cudaPitchedPtr GetLevel(int in_level);

  /// \brief
  /// Returns size of the responses map in bytes
  ///
  /// \return The size of the responses map in bytes.
  size_t GetSize() const;

  /// \brief
  /// Returns pointer to all responses of the pyramid
  ///
  /// \return The pointer to all responses of the pyramid.
  float *GetAllResponses();

  /// \brief
  /// Sets memory to store responses
  /// 
  /// Sets memory to store responses. Size of the used memory should be greater
  /// or equal to size returned by GetSize method.
  /// \param[in] in_memory The pointer to memory.
  void SetMemory(float *in_memory);

  /// \brief
  /// Sets parameters of the response maps on each level
  ///
  /// \param[in] in_width_array  The array of numbers of rows in each level of pyramid.
  /// \param[in] in_height_array The array of numbers of columns in each level of pyramid.
  void SetPyramidParameters(vector<int> in_width_array,
    vector<int> in_height_array);

private:
  /// pointer to memory with responses
  float *m_memory;

  /// array of byte indices that specifies start of the corresponded level of pyramid.
  vector<size_t> m_start_array;

  /// array of widths of maps on each level of the pyramid
  vector<size_t> m_width_array;

  /// array of heights of maps on each level of the pyramid
  vector<size_t> m_height_array;

  /// array of number of bytes between consecutive rows on each level of pyramid.
  vector<size_t> m_pitch_array;

  /// number of bytes used for responses
  size_t m_size;

  /// \brief
  /// Checks that all parameters are set
  ///
  /// \return true if all parameters are set and false otherwise
  bool isSet() const;
};