#ifndef __IHOG_ENGINE__
#define __IHOG_ENGINE__

#include "HOGResult.h"
#include "HOGImage.h"
#include <memory>
#include <string>

namespace HOG
{
#ifdef _WIN32
  class __declspec(dllexport) IHOGEngine
#else
  class IHOGEngine
#endif
	{
	public:
		HOGResult* formattedResults;
		HOGResult* nmsResults;

		bool formattedResultsAvailable;
		int formattedResultsCount;

		bool nmsResultsAvailable;
		int nmsResultsCount;

		virtual void InitializeHOG(int iw, int ih, float svmBias, float* svmWeights, int svmWeightsCount) = 0;
		virtual void InitializeHOG(int iw, int ih, std::string fileName) = 0;

		virtual void FinalizeHOG() = 0;

		virtual void BeginProcess(HOGImage* hostImage, int _minx = -1, int _miny = -1, int _maxx = -1, int _maxy = -1,
			float minScale = -1.0f, float maxScale = -1.0f) = 0;
		virtual void EndProcess() = 0;

    /// \brief
    /// Finalizes processing of the previous image and starts processing of the
    /// current image in an asynchronous way.
    /// 
    /// \param[in]  in_image            The next image to process.
    /// \param[in]  in_min_x            The location of a leftmost pixel to process.
    /// \param[in]  in_min_y            The location of a topmost pixel to process.
    /// \param[in]  in_max_x            The location of a rightmost pixel to process.
    /// \param[in]  in_max_y            The location of a bottommost pixel to process.
    /// \param[in]  in_min_scale        The minimal scale to process.
    /// \param[in]  in_max_scale        The maximal scale to process.
    virtual void ContinueProcess(HOGImage* in_image,
      int in_min_x = -1, int in_min_y = -1,
      int in_max_x = -1, int in_max_y = -1,
      float in_min_scale = -1.0f, float in_max_scale = -1.0f) = 0;

		IHOGEngine() { }
		virtual ~IHOGEngine() {}
	};

#ifdef _WIN32
  IHOGEngine __declspec(dllexport) *GetInstance();
#else
   IHOGEngine *GetInstance();
#endif
}

#endif
