CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(fasterHOG)

#set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_HOME_DIRECTORY}/bin)

FIND_PACKAGE(CUDA REQUIRED)
FIND_PACKAGE(OpenCV REQUIRED)

INCLUDE(FindCUDA)

INCLUDE_DIRECTORIES(${CUDA_TOOLKIT_INCLUDE})
INCLUDE_DIRECTORIES(lib/cutil)
INCLUDE_DIRECTORIES(include)

FILE(GLOB SOURCES "src/*.cu" "src/*.cpp" "src/*.c" "src/*.h" "include/*.h")
FILE(GLOB HEADERS "src/*.h" "include/*.h")
CUDA_ADD_LIBRARY(fasterHOG SHARED ${SOURCES} ${HEADERS})

LIST(APPEND CMAKE_CXX_FLAGS "-std=c++0x -O3 -ffast-math -Wall")

LIST(APPEND CUDA_NVCC_FLAGS --compiler-options -fno-strict-aliasing -lineinfo -use_fast_math -Xptxas -dlcm=cg)
LIST(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_30,code=sm_30)
LIST(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_35,code=sm_35)
LIST(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_52,code=sm_52)

TARGET_LINK_LIBRARIES(fasterHOG ${CUDA_LIBRARIES})

ADD_EXECUTABLE(demo "samples/main.cpp" ${HEADERS})
TARGET_LINK_LIBRARIES(demo fasterHOG ${OpenCV_LIBS})

ADD_SUBDIRECTORY(python)
#get_cmake_property(_variableNames VARIABLES)
#foreach (_variableName ${_variableNames})
#    message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()

