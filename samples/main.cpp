#include <cstdlib>
#include "IHOGEngine.h"
#include <opencv/highgui.h>
#include <opencv/cv.hpp>
#include <iostream>

#include <fstream>

using std::vector;

struct Bbox
{
  /************************************
  * Public fields
  *************************************/
  int x, y, width, height;
  float scale;
  float score;

  /************************************
  * Constructors and Destructor
  *************************************/
  Bbox(const int in_x = 0, const int in_y = 0, const int in_width = 0,
    const int in_height = 0, const float in_scale = 0.f,
    const float in_score = 0.f) : x(in_x), y(in_y), width(in_width),
  height(in_height), scale(in_scale), score(in_score)
  {}
};

void DrawDetections(cv::Mat &io_img, vector<Bbox> &in_detection_array);

void copyFrameToHOGImage(const cv::Mat &in_image, HOG::HOGImage &io_image);

void ConstructBboxes(std::vector<Bbox> &out_detections);

void ConstructBbox(unsigned int in_idx, Bbox &out_bbox);

void WriteDetections(const char *in_file_name, vector<vector<Bbox>> &in_detection_array);

int main(int argc, char *argv[])
{
  int width = 1920;
  int height = 1080;
  HOG::GetInstance()->InitializeHOG(width, height,
    "head_W24x24_C4x4_N2x2_G4x4_HeadSize16x16.alt");
  HOG::HOGImage im_to_process(width, height);

  bool is_visualize = true;
  if (argc > 1)
    is_visualize = atoi(argv[1]) > 0;

  const char *input_img_template = "../data/src/frame%04d.jpg";
  const char *output_img_template = "../data/res/frame%04d.jpg";
  const char *result_file_name = "../data/detections.txt";
  char *buffer = new char[strlen(input_img_template) +
      strlen(output_img_template) + 10];

  cv::Mat img[2];
  vector<vector<Bbox>> detectionArray(0);

  int iFrame = 0;
  int idx = 0;
  // start processing
  sprintf(buffer, input_img_template, iFrame);
  img[0] = cv::imread(buffer);
  if (img[0].data == nullptr)
  {
    fprintf(stderr, "Cannot read first image %s!\n", buffer);
    exit(1);
  }
  copyFrameToHOGImage(img[idx], im_to_process);
  HOG::GetInstance()->BeginProcess(&im_to_process);

  // idicator of precence other frames
  bool isContinue = true;

  while (isContinue)
  {
    // read next frame
    int next_idx = (idx + 1) % 2;
    sprintf(buffer, input_img_template, iFrame + 1);
    cv::Mat &next_ref = img[next_idx];
    cv::Mat &prev_ref = img[idx];
    next_ref = cv::imread(buffer);

    if ((isContinue = (next_ref.data != nullptr))) // next frame is presented
    {
      copyFrameToHOGImage(next_ref, im_to_process);
      HOG::GetInstance()->ContinueProcess(&im_to_process);
    }
    else  // end of sequence
      HOG::GetInstance()->EndProcess();

    // construct detections
    detectionArray.push_back(vector<Bbox>(0));
    ConstructBboxes(*detectionArray.rbegin());

    // draw result
    if (is_visualize)
    {
      DrawDetections(prev_ref, *detectionArray.rbegin());
      sprintf(buffer, output_img_template, iFrame);
      cv::imwrite(buffer, prev_ref);
    }
    idx = next_idx;
    printf("frame %d was processed!\n", iFrame);
    iFrame++;
  }

  WriteDetections(result_file_name, detectionArray);

  delete [] buffer;
  HOG::GetInstance()->FinalizeHOG();
  return 0;
}

void DrawDetections(cv::Mat &io_img, vector<Bbox> &in_detection_array)
{
  int num_detections = in_detection_array.size();
  for (int idx = 0; idx < num_detections; ++idx)
  {
    Bbox &detection = in_detection_array[idx];
    cv::Rect rect(detection.x, detection.y, detection.width, detection.height);
    cv::Scalar color(0, 0, 255);
    cv::rectangle(io_img, rect, color);
  }
}

void copyFrameToHOGImage(const cv::Mat &in_image, HOG::HOGImage &io_image)
{
  int num_channels = in_image.channels();

  unsigned char *in_row = in_image.data;
  unsigned char *out_row = io_image.pixels;
  unsigned char *in_elem, *out_elem;
  for (int row_idx = 0; row_idx < in_image.rows; ++row_idx)
  {
    in_elem = in_row;
    out_elem = out_row;
    for (int col_idx = 0; col_idx < in_image.cols; ++col_idx)
    {
      for (int channel_idx = 0; channel_idx < num_channels; ++channel_idx)
        out_elem[channel_idx] = in_elem[channel_idx];
      in_elem += num_channels;
      out_elem += 4;
    }
    in_row += in_image.step;
    out_row += io_image.width * 4;
  }
}

void ConstructBboxes(std::vector<Bbox> &out_detections)
{
  auto init_size = out_detections.size();
  auto num_detections = static_cast<unsigned int>(HOG::GetInstance()->nmsResultsCount);

  out_detections.resize(init_size + num_detections);

  for (auto idx = 0U; idx < num_detections; idx++)
    ConstructBbox(idx, out_detections[init_size + idx]);
}

void ConstructBbox(unsigned int in_idx, Bbox &out_bbox)
{
  const float factor = 1.49f;
  float init_width = 
    static_cast<float>(HOG::GetInstance()->nmsResults[in_idx].width);
  float init_height =
    static_cast<float>(HOG::GetInstance()->nmsResults[in_idx].height);
  out_bbox.width = static_cast<int>(init_width / factor);
  out_bbox.height = static_cast<int>(init_height / factor);

  out_bbox.x =
    static_cast<int>(HOG::GetInstance()->nmsResults[in_idx].x +
    (init_width - out_bbox.width) / 2.f);
  out_bbox.y =
    static_cast<int>(HOG::GetInstance()->nmsResults[in_idx].y +
    (init_height - out_bbox.height) / 2.f);

  out_bbox.scale = HOG::GetInstance()->nmsResults[in_idx].scale;
  out_bbox.score = HOG::GetInstance()->nmsResults[in_idx].score;
}

void WriteDetections(const char *in_file_name, vector<vector<Bbox>> &in_detection_array)
{
  std::ofstream file(in_file_name);
  if (!file.is_open())
  {
    std::cerr << "Cannot open file " << in_file_name << " for writing!" << std::endl;
    exit(-1);
  }
  size_t num_frames = in_detection_array.size();
  for (size_t frame_idx = 0; frame_idx < num_frames; ++frame_idx)
  {
    size_t num_objects = in_detection_array[frame_idx].size();
    file << frame_idx << " " << num_objects << std::endl;
    for (size_t obj_idx = 0; obj_idx < num_objects; ++obj_idx)
    {
      Bbox &cur_bbox = in_detection_array[frame_idx][obj_idx];
      file << cur_bbox.x << " " << cur_bbox.y << " " << cur_bbox.width << " " << cur_bbox.height <<
        " " << cur_bbox.scale << " " << cur_bbox.score << std::endl;
    }
  }
}
