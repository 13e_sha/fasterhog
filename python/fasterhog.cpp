/* BOOST */
#define BOOST_PYTHON_STATIC_LIB
#include <boost/python.hpp>
using namespace boost::python;

/* NUMPY */
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarrayobject.h>

#include <iostream>
#include <vector>

#include "IHOGEngine.h"

typedef unsigned char uchar;

template<class T>
inline T* make_shift(T *in_ptr, size_t in_shift)
{
    return (T*)((char*)in_ptr + in_shift);
}

template<class T>
struct PitchedPtr
{
    size_t num_rows;
    size_t num_cols;
    size_t num_channels;
    size_t pitch;
    T *ptr;

    T *getRow(size_t in_row)
    {
        return make_shift(ptr, in_row * pitch);
    }

    T *getElem(size_t in_row, size_t in_column)
    {
        return getRow(in_row) + in_column;
    }
};

void parseImageParameter(const object &in_param, PitchedPtr<uchar> &out_matrix)
{
    PyArrayObject *param_obj = (PyArrayObject*)in_param.ptr();
    PyArray_Descr *param_descr = PyArray_DESCR(param_obj);
    if (param_descr->type != 'B')
    { std::cerr << "Wrong ndarray type, it should be 8bit unsigned integer" << std::endl; abort();}
    int dims = PyArray_NDIM(param_obj);
    if ((dims != 2) && (dims != 3))
    { std::cerr << "Wrong number of dimensions" << std::endl; abort();}
    size_t row_stride = PyArray_STRIDE(param_obj, 0);
    size_t col_stride = PyArray_STRIDE(param_obj, 1);
    size_t channel_stride = dims == 2 ? 1 : PyArray_STRIDE(param_obj, 2);
    if ((row_stride < col_stride) || (col_stride < channel_stride))
        throw("Wrong ndarray format. It should have C-contiguous order.");
    out_matrix.num_rows = PyArray_DIMS(param_obj)[0];
    out_matrix.num_cols = PyArray_DIMS(param_obj)[1];
    out_matrix.num_channels = col_stride;
    out_matrix.ptr = static_cast<uchar*>(PyArray_DATA(param_obj));
    out_matrix.pitch = row_stride;
}

void constructFloatMatrix(size_t in_num_rows, size_t in_num_cols,
       PitchedPtr<float> &io_matrix, object &out_p_matrix)
{
    Py_intptr_t shape[2] = {in_num_rows, in_num_cols};
    PyObject *pyObj = PyArray_SimpleNew(2, shape, NPY_FLOAT);
    // construct boost object
    out_p_matrix = object(handle<>(pyObj));

    // interpret pyObject as array
    PyArrayObject *obj = (PyArrayObject*)pyObj;

    io_matrix.num_rows = in_num_rows;
    io_matrix.num_cols = in_num_cols;
    io_matrix.pitch = PyArray_STRIDE(obj, 0);
    io_matrix.ptr = static_cast<float*>(PyArray_DATA(obj));
}

void ConstructBbox(unsigned int in_idx, float *out_bbox)
{
  const float factor = 1.49f;
  float init_width = 
    static_cast<float>(HOG::GetInstance()->nmsResults[in_idx].width);
  float init_height =
    static_cast<float>(HOG::GetInstance()->nmsResults[in_idx].height);
  // width
  out_bbox[2] = init_width / factor;
  // height
  out_bbox[3] = init_height / factor;

  // x
  out_bbox[0] =
      HOG::GetInstance()->nmsResults[in_idx].x + (init_width - out_bbox[2]) / 2.f;
  // y
  out_bbox[1] = 
      HOG::GetInstance()->nmsResults[in_idx].y + (init_height - out_bbox[3]) / 2.f;

  // scale
  out_bbox[4] = HOG::GetInstance()->nmsResults[in_idx].scale;
  // score
  out_bbox[5] = HOG::GetInstance()->nmsResults[in_idx].score;
}

void ConstructBboxes(PitchedPtr<float> &io_bbox_array)
{
  for (size_t idx = 0U; idx < io_bbox_array.num_rows; idx++)
    ConstructBbox(idx, io_bbox_array.getRow(idx));
}

void copyFrameToHOGImage(PitchedPtr<uchar> &in_image, HOG::HOGImage &io_image)
{
  size_t num_channels = in_image.num_channels;
  unsigned char *in_row = in_image.ptr;
  unsigned char *out_row = io_image.pixels;
  unsigned char *in_elem, *out_elem;
  for (size_t row_idx = 0; row_idx < in_image.num_rows; ++row_idx)
  {
    in_elem = in_row;
    out_elem = out_row;
    for (size_t col_idx = 0; col_idx < in_image.num_cols; ++col_idx)
    {
      for (size_t channel_idx = 0; channel_idx < num_channels; ++channel_idx)
        out_elem[channel_idx] = in_elem[channel_idx];
      for (size_t channel_idx = num_channels; channel_idx < 4; ++channel_idx)
        out_elem[channel_idx] = 0;
      in_elem += num_channels;
      out_elem += 4;
    }
    in_row += in_image.pitch;
    out_row += io_image.width * 4;
  }
}

void initDetector(int in_width, int in_height, std::string in_model_file)
{
  HOG::GetInstance()->InitializeHOG(in_width, in_height,
      in_model_file);
}

void start_process(const object &in_image)
{
    // input image
    PitchedPtr<uchar> img;
    // get pointer to the input image
    parseImageParameter(in_image, img);

    HOG::HOGImage im_to_process(img.num_cols, img.num_rows);
    copyFrameToHOGImage(img, im_to_process);
    HOG::GetInstance()->BeginProcess(&im_to_process);
}

object continue_process(const object &in_image)
{
    // input image
    PitchedPtr<uchar> img;
    // result bounding boxes
    PitchedPtr<float> bbox_array;

    // get pointer to the input image
    parseImageParameter(in_image, img);

    HOG::HOGImage im_to_process(img.num_cols, img.num_rows);
    copyFrameToHOGImage(img, im_to_process);
    HOG::GetInstance()->ContinueProcess(&im_to_process);

    object result_obj;
    size_t num_bboxes = HOG::GetInstance()->nmsResultsCount;
    constructFloatMatrix(num_bboxes, 6, bbox_array, result_obj);
    ConstructBboxes(bbox_array);

    return result_obj;
}

object stop_process()
{
    // result bounding boxes
    PitchedPtr<float> bbox_array;
    HOG::GetInstance()->EndProcess();

    object result_obj;
    size_t num_bboxes = HOG::GetInstance()->nmsResultsCount;
    constructFloatMatrix(num_bboxes, 6, bbox_array, result_obj);
    ConstructBboxes(bbox_array);

    return result_obj;
}

void release()
{
  HOG::GetInstance()->FinalizeHOG();
}

object detect_head(const object &in_image, std::string in_model_file)
{
    // input image
    PitchedPtr<uchar> img;
    // result bounding boxes
    PitchedPtr<float> bbox_array;

    // get pointer to the input image
    parseImageParameter(in_image, img);

    // initialize detector
    HOG::GetInstance()->InitializeHOG(img.num_cols, img.num_rows,
      in_model_file);

    HOG::HOGImage im_to_process(img.num_cols, img.num_rows);
    copyFrameToHOGImage(img, im_to_process);
    HOG::GetInstance()->BeginProcess(&im_to_process);
    HOG::GetInstance()->EndProcess();

    object result_obj;
    size_t num_bboxes = HOG::GetInstance()->nmsResultsCount;
    constructFloatMatrix(num_bboxes, 6, bbox_array, result_obj);
    ConstructBboxes(bbox_array);

    return result_obj;
}

BOOST_PYTHON_MODULE(pyhog)
{
    import_array();

    def("detect_head", detect_head);
    def("init", initDetector);
    def("release", release);
    def("start", start_process);
    def("next", continue_process);
    def("stop", stop_process);
}
