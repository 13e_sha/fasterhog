cmake_minimum_required(VERSION 2.8)

project(pyhog)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -Wall -march=native")

FILE(GLOB HEADERS "include/*.h")

#find boost & build pyfacesdk
find_package(PythonLibs 2.7 REQUIRED)

include_directories(${PYTHON_INCLUDE_DIRS} /usr/lib/python2.7/site-packages/numpy/core/include ${ADD_PATH}/include)

FIND_PACKAGE(Boost REQUIRED)
FIND_PACKAGE(Boost COMPONENTS python REQUIRED)
set(Boost_USE_STATIC_LIBS ON)
include_directories(${Boost_INCLUDE_DIR})

#LINK_DIRECTORIES("./")

add_library(pyhog SHARED fasterhog.cpp ${HEADERS})
TARGET_LINK_LIBRARIES(pyhog ${Boost_LIBRARIES})
TARGET_LINK_LIBRARIES(pyhog fasterHOG)
set_target_properties(pyhog PROPERTIES PREFIX "")
