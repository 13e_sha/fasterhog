#ifndef __HOG_PADDING__
#define __HOG_PADDING__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  include <windows.h>
#endif

#include <cuda_gl_interop.h>
#include <cutil_inline.h>
#include <cuda.h>
#include "HOGDefines.h"

class HOGPadding
{
public:
  /// \brief
  /// The constructor
  HOGPadding(cudaStream_t in_stream = 0);

  /// \brief
  /// The constructor
  /// 
  /// \param[in] in_padded_width  A width of the image after padding
  /// \param[in] in_padded_height A height of the image after padding
  HOGPadding(int in_padded_width, int in_padded_height, cudaStream_t in_stream = 0);

  /// \brief
  /// The destructor
  ~HOGPadding();

  /// \brief
  /// Initializes object.
  /// 
  /// \param[in] in_padded_width  A width of the image after padding
  /// \param[in] in_padded_height A height of the image after padding
  void Init(int in_padded_width, int in_padded_height, cudaStream_t in_stream = 0);

  /// \brief
  /// Release state of the object
  void Release();

  __host__ void PadHostImage(uchar4* registeredImage,
    float4 *paddedRegisteredImage, int minx, int miny, int maxx, int maxy,
    int in_hWidth, int in_hHeight, int in_avSizeX, int in_avSizeY,
    int in_marginX, int in_marginY,
    int &out_hPaddingSizeX, int &out_hPaddingSizeY, 
    int &out_hPaddedWidth, int &out_hPaddedHeight,
    int &out_hWidthROI, int &out_hHeightROI);
private:
  /// Indicator of the state to be set.
  bool m_is_set;
  
  /// internal buffer
  uchar4* m_buffer;

  /// stream for operations
  cudaStream_t m_stream;
};

#endif
