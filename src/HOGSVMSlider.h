#ifndef __HOG_SVM_SLIDER__
#define __HOG_SVM_SLIDER__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  include <windows.h>
#endif

#include <cuda_gl_interop.h>
#include <cutil_inline.h>
#include <cuda.h>

#include "HOGDefines.h"

class HOGSVMSlider
{
public:
  /// \brief
  /// The constructor
  ///
  /// \param[in] svmBias                 The bias term.
  /// \param[in] svmWeights              The pointer to the SVM weights.
  /// \param[in] svmWeightsCount         The number of SVM weights.
  /// \param[in] num_blocks_per_window_x The number of blocks in a window along X direction.
  /// \param[in] num_blocks_per_window_y The number of blocks in a window along Y direction.
  /// \param[in] num_cells_per_block_x   The number of cells in a block along X direction.
  /// \param[in] num_cells_per_block_x   The number of cells in a block along Y direction.
  /// \param[in] noHistogramBins         The number of bins in a histogram.
  /// \param[in] in_stream               The stream to make computations.
  HOGSVMSlider(cudaStream_t in_stream = 0);

  /// \brief
  /// The destructor
  ~HOGSVMSlider();

  /// \brief
  /// Set weights of the SVM.
  ///
  /// \param[in] in_bias         The bias term.
  /// \param[in] in_weights      The array of weights.
  /// \param[in] in_num_elems    The number of weights.
  /// \param[in] in_num_blocks_x The number of blocks per window along X direction.
  /// \param[in] in_num_blocks_y The number of blocks per window along Y direction.
  /// \param[in] in_block_size_x The number of cells per block along X direction.
  /// \param[in] in_block_size_y The number of cells per block along Y direction.
  /// \param[in] in_num_bins     The number of bins in the histogram
  __host__ static void SetWeights(float in_bias, float* in_weights,
    int in_num_elems, int in_num_blocks_x, int in_num_blocks_y,
    int in_block_size_x, int in_block_size_y, int in_num_bins);

  /// \brief
  /// Releases weights of the SVM.
  __host__ static void ReleaseWeights();

  /// \brief
  /// Reset weights of the SVM
  ///
  /// \param[in] in_bias         The bias term.
  /// \param[in] in_weights      The array of weights.
  /// \param[in] in_num_elems    The number of weights.
  /// \param[in] in_num_blocks_x The number of blocks per window along X direction.
  /// \param[in] in_num_blocks_y The number of blocks per window along Y direction.
  /// \param[in] in_block_size_x The number of cells per block along X direction.
  /// \param[in] in_block_size_y The number of cells per block along Y direction.
  /// \param[in] in_num_bins     The number of bins in the histogram
  __host__ static void UpdateSVMWeights(float in_bias, float* in_weights,
    int in_num_elems, int in_num_blocks_x, int in_num_blocks_y,
    int in_block_size_x, int in_block_size_y, int in_num_bins);

  /// \brief
  /// Returns number of windows for the specified number of blocks
  ///
  /// \param[in] in_num_blocks_x            The number of blocks along X direction
  /// \param[in] in_num_blocks_y            The number of blocks along Y direction
  /// \param[in] in_num_blocks_per_window_x The number of blocks in window along X direction
  /// \param[in] in_num_blocks_per_window_y The number of blocks in window along Y direction
  /// \param[in] in_num_windows_x           The number of windows along X direction
  /// \param[in] in_num_windows_y           The number of windows along Y direction
  __host__ static void CountWindows(int in_num_blocks_x, int in_num_blocks_y,
    int in_num_blocks_per_window_x, int in_num_blocks_per_window_y,
    int &out_num_windows_x, int &out_num_windows_y);

  __host__ void LinearSVMEvaluation(cudaPitchedPtr svmScores,
    cudaPitchedPtr blockHistograms, int noHistogramBins,
    int in_window_size_x, int in_window_size_y);
private:

  static float svmBias;
  static cudaArray *svmArray;
  static cudaChannelFormatDesc channelDescSVM;

  /// number of objects of this class.
  static unsigned int m_num_objects;

  /// indicator of the state is set.
  static bool m_is_set;

  cudaStream_t m_stream;

};
#endif
