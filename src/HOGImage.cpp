/*
 * HOGImage.cpp
 *
 *  Created on: May 14, 2009
 *      Author: viprad
 */

#include "HOGImage.h"

#include <stdlib.h>
#include <string.h>

#include <stdio.h>

using namespace HOG;

HOGImage::HOGImage(int width, int height)
{
	this->width = width;
	this->height = height;

	isLoaded = false;
	this->pixels = (unsigned char*) malloc(sizeof(unsigned char) * 4 * width * height);
	memset(this->pixels, 0, sizeof(unsigned char) * 4 * width * height);
}

HOGImage::HOGImage(int width, int height, unsigned char* pixels)
{
	this->width = width;
	this->height = height;

	this->pixels = (unsigned char*) malloc(sizeof(unsigned char) * 4 * width * height);
	memcpy(this->pixels, pixels, sizeof(unsigned char) * 4 * width * height);

	isLoaded = true;
}

HOGImage::~HOGImage()
{
	free(pixels);
}
