#ifndef __HOG_CONVOLUTION__
#define __HOG_CONVOLUTION__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  include <windows.h>
#endif

#include <cuda_gl_interop.h>
#include <cutil_inline.h>
#include <cuda.h>

#include "HOGDefines.h"
#include "HOGUtils.h"

class HOGConvolution
{
public:
  __host__ HOGConvolution(int in_width, int in_height,
    bool in_is_use_grayscale, cudaStream_t in_stream = 0);

  __host__ ~HOGConvolution();

  __host__ void SetConvolutionSize(int width, int height);

  __host__ void ComputeColorGradients4to2(cudaPitchedPtr inputImage, cudaPitchedPtr outputImage);
  __host__ void ComputeColorGradients1to2(cudaPitchedPtr inputImage, cudaPitchedPtr outputImage);

private:
  dim3 blockGridRows;
  dim3 blockGridColumns;
  dim3 threadBlockRows;
  dim3 threadBlockColumns;

  cudaStream_t m_stream;

  cudaPitchedPtr convBuffer;

  bool convUseGrayscale;

  int convWidth;
  int convHeight;
  
  /// number of constructed objects
  static unsigned int m_num_objects;

  ///
  __host__ static void initKernel();
};

#endif
