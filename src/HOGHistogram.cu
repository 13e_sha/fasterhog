#include "HOGHistogram.h"
#include "HOGUtils.h"

#include <cassert>
#include <fstream>
#include <iostream>

#define TEST

#define NROWS 1

__device__ __constant__ float cenBound[3], halfBin[3], bandWidth[3], oneHalf = 0.5f;

texture<float, 1, cudaReadModeElementType> texGauss;
const int MAX_CONSTANT = 64;
__device__ __constant__ float gauss[MAX_CONSTANT];
__device__ __constant__ float factor0[MAX_CONSTANT];
__device__ __constant__ float factor1[MAX_CONSTANT];
__device__ __constant__ float factor2[MAX_CONSTANT];
__device__ __constant__ float factor3[MAX_CONSTANT];
__device__ __constant__ int offset0[MAX_CONSTANT];
__device__ __constant__ int offset1[MAX_CONSTANT];
__device__ __constant__ int offset2[MAX_CONSTANT];
__device__ __constant__ int offset3[MAX_CONSTANT];
__device__ __constant__ char mask[MAX_CONSTANT];
cudaArray* HOGHistogram::gaussArray;
cudaChannelFormatDesc HOGHistogram::channelDescGauss;
unsigned int HOGHistogram::m_num_objects = 0;
bool HOGHistogram::m_is_set = false;

extern __shared__ float allShared[];

/// \brief
/// Computes floor, ceil and fractional values of the input float value
///
/// \param[in]  in_value       The input float value
/// \param[out] out_floor      The floor of the input value
/// \param[out] out_ceil       The ceil of the input value
/// \param[out] out_floor_frac The difference between the input value and its floor
/// \param[out] out_ceil_frac  The difference between the ceil and the input values
inline __host__ void computeApproximationsHost(float in_value,
                                               int &out_floor, int &out_ceil,
                                               float &out_floor_frac,
                                               float &out_ceil_frac)
{
  out_floor = floorf(in_value);
  out_ceil = out_floor + 1;
  out_floor_frac = in_value - out_floor;
  out_ceil_frac = 1 - out_floor_frac;
}

inline __host__ bool isValueInRangeHost(int in_value, int in_range)
{
  return (in_value >= 0) && (in_value < in_range);
}

__host__ void HOGHistogram::InitHistogram(int in_cellSizeX, int in_cellSizeY,
                                          int in_blockSizeX, int in_blockSizeY,
                                          int in_num_bins, float in_wtscale)
{
  CloseHistogram();

  int num_block_elems_x = in_cellSizeX * in_blockSizeX;
  int num_block_elems_y = in_cellSizeY * in_blockSizeY;

  int num_block_elems = num_block_elems_x * num_block_elems_y;

  float var2x = num_block_elems_x / (2 * in_wtscale);
  float var2y = num_block_elems_y / (2 * in_wtscale);
	var2x *= var2x * 2;
  var2y *= var2y * 2;

  float centerX = num_block_elems_x / 2.0f;
  float centerY = num_block_elems_y / 2.0f;

  float* weights = new float[num_block_elems];

  // reordered version of weights
  size_t idx = 0;
  for (size_t row_idx = 0; row_idx < in_cellSizeY; ++row_idx)
    for (size_t cell_y_idx = 0; cell_y_idx < in_blockSizeY; ++cell_y_idx)
      for (size_t cell_x_idx = 0; cell_x_idx < in_blockSizeX; ++cell_x_idx)
        for (size_t column_idx = 0; column_idx < in_cellSizeY; ++column_idx)
        {
          size_t y = row_idx + cell_y_idx * in_cellSizeY;
          size_t x = column_idx + cell_x_idx * in_cellSizeX;

          float tx = x - centerX;
          float ty = y - centerY;

          tx *= tx / var2x;
          ty *= ty / var2y;

          weights[idx++] = exp(-(tx + ty));
        }

	channelDescGauss = cudaCreateChannelDesc<float>();

  cutilSafeCall(cudaMallocArray(&gaussArray, &channelDescGauss,
    num_block_elems, 1));
  cutilSafeCall(cudaMemcpyToArray(gaussArray, 0, 0, weights,
    sizeof(float) * num_block_elems, cudaMemcpyHostToDevice));
  if (num_block_elems <= MAX_CONSTANT)
  {
    cutilSafeCall(cudaMemcpyToSymbol(gauss, weights,
      num_block_elems * sizeof(float), 0, cudaMemcpyHostToDevice));
  }

  delete[] weights;

	float h_cenBound[3], h_halfBin[3], h_bandWidth[3];
	h_cenBound[0] = num_block_elems_x / 2.0f;
	h_cenBound[1] = num_block_elems_y / 2.0f;
	h_cenBound[2] = 180 / 2.0f; //TODO -> can be 360

	h_halfBin[0] = in_blockSizeX / 2.0f;
	h_halfBin[1] = in_blockSizeY / 2.0f;
	h_halfBin[2] = in_num_bins / 2.0f;

	h_bandWidth[0] = (float)in_cellSizeX;
  h_bandWidth[0] = 1.0f / h_bandWidth[0];
	h_bandWidth[1] = (float)in_cellSizeY;
  h_bandWidth[1] = 1.0f / h_bandWidth[1];
  h_bandWidth[2] = 180.0f / (float)in_num_bins;
  h_bandWidth[2] = 1.0f / h_bandWidth[2]; //TODO -> can be 360

	cutilSafeCall(cudaMemcpyToSymbol(cenBound, h_cenBound, 3 * sizeof(float),
    0, cudaMemcpyHostToDevice));
	cutilSafeCall(cudaMemcpyToSymbol(halfBin, h_halfBin, 3 * sizeof(float),
    0, cudaMemcpyHostToDevice));
	cutilSafeCall(cudaMemcpyToSymbol(bandWidth, h_bandWidth, 3 * sizeof(float),
    0, cudaMemcpyHostToDevice));

	cutilSafeCall(cudaBindTextureToArray(texGauss, gaussArray, channelDescGauss));

    float *factor0_host = new float[num_block_elems];
  float *factor1_host = new float[num_block_elems];
  float *factor2_host = new float[num_block_elems];
  float *factor3_host = new float[num_block_elems];

  int *offset0_host = new int[num_block_elems];
  int *offset1_host = new int[num_block_elems];
  int *offset2_host = new int[num_block_elems];
  int *offset3_host = new int[num_block_elems];

  char *mask_host = new char[num_block_elems];

  idx = 0;
  float pIx_base = h_halfBin[0] - 0.5f + (0.5f - h_cenBound[0]) * h_bandWidth[0];
  float pIy_base = h_halfBin[1] - 0.5f + (0.5f - h_cenBound[1]) * h_bandWidth[1];
  for (size_t row_idx = 0; row_idx < in_cellSizeY; ++row_idx)
    for (size_t cell_y_idx = 0; cell_y_idx < in_blockSizeY; ++cell_y_idx)
      for (size_t cell_x_idx = 0; cell_x_idx < in_blockSizeX; ++cell_x_idx)
        for (size_t column_idx = 0; column_idx < in_cellSizeY; ++column_idx)
        {
          size_t y = row_idx + cell_y_idx * in_cellSizeY;
          size_t x = column_idx + cell_x_idx * in_cellSizeX;

          float pIx = pIx_base + x * h_bandWidth[0];
          float pIy = pIy_base + y * h_bandWidth[1];

          int fIy, cIy, fIx, cIx;
          float fy, cy, fx, cx;

          computeApproximationsHost(pIy, fIy, cIy, fy, cy);
          computeApproximationsHost(pIx, fIx, cIx, fx, cx);
          factor0_host[idx] = cx * cy;
          factor1_host[idx] = cx * fy;
          factor2_host[idx] = fx * cy;
          factor3_host[idx] = fx * fy;

          offset0_host[idx] = (fIx + fIy * in_blockSizeY) * in_num_bins;
          offset1_host[idx] = (fIx + cIy * in_blockSizeY) * in_num_bins;
          offset2_host[idx] = (cIx + fIy * in_blockSizeY) * in_num_bins;
          offset3_host[idx] = (cIx + cIy * in_blockSizeY) * in_num_bins;

          char flag = 0;

          bool lowervalidy = isValueInRangeHost(fIy, in_blockSizeY);
          bool uppervalidy = isValueInRangeHost(cIy, in_blockSizeY);

          bool lowervalidx = isValueInRangeHost(fIx, in_blockSizeX);
          bool uppervalidx = isValueInRangeHost(cIx, in_blockSizeX);

          if (lowervalidx && lowervalidy)
            flag |= 1;
          if (lowervalidx && uppervalidy)
            flag |= 2;
          if (uppervalidx && lowervalidy)
            flag |= 4;
          if (uppervalidx && uppervalidy)
            flag |= 8;

          mask_host[idx] = flag;

          idx++;
        }

  if (num_block_elems <= MAX_CONSTANT)
  {
    cutilSafeCall(cudaMemcpyToSymbol(factor0, factor0_host,
      num_block_elems * sizeof(float), 0, cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpyToSymbol(factor1, factor1_host,
      num_block_elems * sizeof(float), 0, cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpyToSymbol(factor2, factor2_host,
      num_block_elems * sizeof(float), 0, cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpyToSymbol(factor3, factor3_host,
      num_block_elems * sizeof(float), 0, cudaMemcpyHostToDevice));

    cutilSafeCall(cudaMemcpyToSymbol(offset0, offset0_host,
      num_block_elems * sizeof(int), 0, cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpyToSymbol(offset1, offset1_host,
      num_block_elems * sizeof(int), 0, cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpyToSymbol(offset2, offset2_host,
      num_block_elems * sizeof(int), 0, cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpyToSymbol(offset3, offset3_host,
      num_block_elems * sizeof(int), 0, cudaMemcpyHostToDevice));

    cutilSafeCall(cudaMemcpyToSymbol(mask, mask_host,
      num_block_elems * sizeof(char), 0, cudaMemcpyHostToDevice));
  }

  delete [] offset3_host;
  delete [] offset2_host;
  delete [] offset1_host;
  delete [] offset0_host;

  delete [] factor3_host;
  delete [] factor2_host;
  delete [] factor1_host;
  delete [] factor0_host;

  m_is_set = true;
}

__host__ void HOGHistogram::CloseHistogram()
{
  if (m_is_set)
  {
    cutilSafeCall(cudaUnbindTexture(texGauss));
    cudaFreeArray(gaussArray);  
    m_is_set = false;
  }
}

__host__ HOGHistogram::HOGHistogram(cudaStream_t in_steam)
  : m_stream(in_steam)
{
}

__host__ HOGHistogram::~HOGHistogram()
{
}

/// \brief
/// Computes floor, ceil and fractional values of the input float value
///
/// \param[in]  in_value       The input float value
/// \param[out] out_floor      The floor of the input value
/// \param[out] out_ceil       The ceil of the input value
/// \param[out] out_floor_frac The difference between the input value and its floor
/// \param[out] out_ceil_frac  The difference between the ceil and the input values
inline __device__ void computeApproximations(float in_value,
                                             int &out_floor, int &out_ceil,
                                             float &out_floor_frac,
                                             float &out_ceil_frac)
{
  out_floor = floorf(in_value);
  out_ceil = out_floor + 1;
  out_floor_frac = in_value - out_floor;
  out_ceil_frac = 1 - out_floor_frac;
}

/// \brief
/// Computes floor and fractional values of the input float value
///
/// \param[in]  in_value       The input float value
/// \param[out] out_floor      The floor of the input value
/// \param[out] out_ceil_frac  The difference between the ceil and the input values
inline __device__ void computeFloorApproximations(float in_value,
                                                  int &out_floor,
                                                  float &out_ceil_frac)
{
  out_floor = floorf(in_value);
  out_ceil_frac = 1 - (in_value - out_floor);
}

/// \brief
/// Computes ceil and fractional values of the input float value
///
/// \param[in]  in_value       The input float value
/// \param[out] out_floor      The floor of the input value
/// \param[out] out_ceil       The ceil of the input value
/// \param[out] out_floor_frac The difference between the input value and its floor
/// \param[out] out_ceil_frac  The difference between the ceil and the input values
inline __device__ void computeCeilApproximations(float in_value,
                                                 int &out_ceil,
                                                 float &out_floor_frac)
{
  float floor = floorf(in_value);
  out_ceil = floor + 1;
  out_floor_frac = in_value - floor;
}

/// \brief
/// Reads the current portion of data from the global memory.
///
/// \param[in]     in_gmem             The pointer to the first element to read from the global memory
/// \param[in]     in_gmem_plane_step  The number of bytes between planes in the global memory
/// \param[in,out] io_cache            The data in the shared memory
/// \param[in]     in_cache_plane_step The number of bytes between planes in the shared memory
/// \param[in]     in_thread_idx       The index of the current thread
/// \param[in]     in_num_threads      The number of threads in the block
inline __device__ void readData(cudaPitchedPtr in_gmem,
                                size_t in_gmem_plane_step,
                                cudaPitchedPtr io_cache,
                                size_t in_cache_plane_step,
                                int in_thread_idx, int in_num_threads)
{
  // read data to the cache
  float *gmem_magnitude_base = (float*)in_gmem.ptr;
  float *gmem_angle_base =
    make_step((float*)in_gmem.ptr, in_gmem_plane_step, float);

  float *cache_magnitude_base = (float*)io_cache.ptr;
  float *cache_angle_base =
    make_step(cache_magnitude_base, in_cache_plane_step, float);
  for (int line_idx = 0; line_idx < in_gmem.ysize; ++line_idx)
  {
    for (int read_idx = in_thread_idx; read_idx < in_gmem.xsize; read_idx += in_num_threads)
    {
      cache_magnitude_base[read_idx] = gmem_magnitude_base[read_idx];
      cache_angle_base[read_idx] = gmem_angle_base[read_idx];
    }
    gmem_magnitude_base = make_step(gmem_magnitude_base, in_gmem.pitch, float);
    gmem_angle_base = make_step(gmem_angle_base, in_gmem.pitch, float);

    cache_magnitude_base = make_step(cache_magnitude_base, io_cache.pitch, float);
    cache_angle_base = make_step(cache_angle_base, io_cache.pitch, float);
  }
}

/// \brief
/// Initializes histogram with the zero values
///
/// \param[in,out] io_histogram   The pointer to the histogram
/// \param[in]     in_step        The number of bytes between consecutive elements of the histogram
/// \param[in]     in_num_elems   The number of elements in the histogram
/// \param[in]     in_thread_idx  The index of the current thread
/// \param[in]     in_num_threads The number of threads per histogram
inline __device__ void initHistogram(float* io_histogram, size_t in_step, int in_num_elems,
  int in_thread_idx, int in_num_threads)
{
  size_t step = in_step * in_num_threads;
  float* ptr = make_step(io_histogram, in_thread_idx * in_step, float);
  for (int i = in_thread_idx; i < in_num_elems; i += in_num_threads)
  {
    *ptr = 0;
    ptr = make_step(ptr, step, float);
  }
}

/// \brief
/// Reads the current portion of data from the global memory.
///
/// \param[in]     in_gmem             The pointer to the first element to read from the global memory
/// \param[in]     in_gmem_plane_step  The number of bytes between planes in the global memory
/// \param[in,out] io_cache            The data in the shared memory
/// \param[in]     in_cache_plane_step The number of bytes between planes in the shared memory
/// \param[in]     in_thread_x_idx     The index of the current thread
/// \param[in]     in_thread_y_idx     The index of the current thread
/// \param[in]     in_num_threads_x    The number of threads in the block
/// \param[in]     in_num_threads_y    The number of threads in the block
inline __device__ void readData(cudaPitchedPtr in_gmem,
                                size_t in_gmem_plane_step,
                                cudaPitchedPtr io_cache,
                                size_t in_cache_plane_step,
                                int in_thread_x_idx, int in_thread_y_idx,
                                int in_num_threads_x, int in_num_threads_y)
{
  // pointer to the first line of global memory to read by the thread
  float *gmem_magnitude_base = get_row(in_gmem, in_thread_y_idx, float);
  float *gmem_angle_base;

  // step between consecutive rows to read by the thread
  size_t gmem_step = in_gmem.pitch * in_num_threads_y;
  size_t cache_step = io_cache.pitch * in_num_threads_y;

  // pointer to the first line of shared memory to read by the thread
  float *cache_magnitude_base = get_row(io_cache, in_thread_y_idx, float);
  float *cache_angle_base;
  for (int line_idx = in_thread_y_idx; line_idx < io_cache.ysize; line_idx += in_num_threads_y)
  //for (int line_idx = in_thread_y_idx; line_idx < in_gmem.ysize; line_idx += in_num_threads_y)
  {
    // get pointer to the angles
    gmem_angle_base = make_step(gmem_magnitude_base, in_gmem_plane_step, float);
    cache_angle_base = make_step(cache_magnitude_base, in_cache_plane_step, float);

    //for (int read_idx = in_thread_x_idx; read_idx < in_gmem.xsize; read_idx += in_num_threads_x)
    for (int read_idx = in_thread_x_idx; read_idx < io_cache.xsize; read_idx += in_num_threads_x)
    {
      if ((line_idx < in_gmem.ysize) && (read_idx < in_gmem.xsize))
      {
        cache_magnitude_base[read_idx] = gmem_magnitude_base[read_idx];
        cache_angle_base[read_idx] = halfBin[2] - oneHalf + (gmem_angle_base[read_idx] - cenBound[2]) * bandWidth[2];
      }
      else
      {
        cache_magnitude_base[read_idx] = 0;
        cache_angle_base[read_idx] = 0;
      }
    }

    // go to the next row
    gmem_magnitude_base = make_step(gmem_magnitude_base, gmem_step, float);
    cache_magnitude_base = make_step(cache_magnitude_base, cache_step, float);
  }
}


inline __device__ bool isValueInRange(int in_value, int in_range)
{
  return (in_value >= 0) && (in_value < in_range);
}

inline __device__ void AddToHistogram(float *in_magnitude_ptr,
                                      float *in_angle_ptr, 
                                      float *io_histogram_ptr, int in_num_bins,
                                      size_t in_histogram_step,
                                      int in_column_idx, int in_cmem_idx,
                                      int in_offset, float in_factor)

{
  int fIz, cIz;
  float dz, cz, pIz;

  pIz = in_angle_ptr[in_column_idx];
  computeApproximations(pIz, fIz, cIz, dz, cz);

  cIz %= in_num_bins;
  fIz %= in_num_bins;
  if (fIz < 0) fIz += in_num_bins;
  if (cIz < 0) cIz += in_num_bins;

  float mag = in_magnitude_ptr[in_column_idx];
  mag *= tex1D(texGauss, in_cmem_idx);
  float localValue_cz = mag * cz;
  float localValue_dz = mag * dz;

  *make_step(io_histogram_ptr, (in_offset + fIz) * in_histogram_step, float) +=
    localValue_cz * in_factor;
  *make_step(io_histogram_ptr, (in_offset + cIz) * in_histogram_step, float) +=
    localValue_dz * in_factor;
}

inline __device__ void AddToHistogramConstant(float *in_magnitude_ptr,
                                      float *in_angle_ptr, 
                                      float *io_histogram_ptr, int in_num_bins,
                                      size_t in_histogram_step,
                                      int in_column_idx, int in_cmem_idx,
                                      int in_offset, float in_factor)

{
  int fIz, cIz;
  float dz, cz, pIz;

  pIz = in_angle_ptr[in_column_idx];
  computeApproximations(pIz, fIz, cIz, dz, cz);

  cIz %= in_num_bins;
  fIz %= in_num_bins;
  if (fIz < 0) fIz += in_num_bins;
  if (cIz < 0) cIz += in_num_bins;

  float mag = in_magnitude_ptr[in_column_idx];
  mag *= gauss[in_cmem_idx];
  float localValue_cz = mag * cz;
  float localValue_dz = mag * dz;

  *make_step(io_histogram_ptr, (in_offset + fIz) * in_histogram_step, float) +=
    localValue_cz * in_factor;
  *make_step(io_histogram_ptr, (in_offset + cIz) * in_histogram_step, float) +=
    localValue_dz * in_factor;
}

/// \brief
/// Writes histograms to the global memory
///
/// \param[out] in_blockHistograms The array of histograms in the global memory
/// \param[in]  in_histogram_ptr   The pointer to the histogram in the shared memory
/// \param[in]  in_histogram_step  The number of elementes in the histogram
/// \param[in]  in_x_idx           The index of the histogram in the global memory along X direction
/// \param[in]  in_y_idx           The index of the histogram in the global memory along Y direction
/// \param[in]  in_thread_idx      The index of the thread to pull of threads to write the histogram
/// \param[in]  in_num_threads     The number of threads to write the histogram
inline __device__ void writeData(cudaPitchedPtr in_blockHistograms,
                                 float *in_histogram_ptr,
                                 size_t in_histogram_step,
                                 int in_histogram_size,
                                 int in_x_idx, int in_y_idx,
                                 int in_thread_idx, int in_num_threads)
{
  bool is_valid = (in_x_idx < in_blockHistograms.xsize) &&
    (in_y_idx < in_blockHistograms.ysize);
  if (is_valid)
  {
    size_t gmem_step = in_blockHistograms.ysize * in_blockHistograms.pitch;
    float *gmem_bin_ptr =
      get_elem(in_blockHistograms, in_y_idx, in_x_idx, float);
    gmem_bin_ptr = make_step(gmem_bin_ptr, in_thread_idx * gmem_step, float);
    gmem_step = in_num_threads * gmem_step;
    float *histogram_bin_ptr =
      make_step(in_histogram_ptr, in_thread_idx * in_histogram_step, float);
    size_t shared_step = in_num_threads * in_histogram_step;
    for (int i = in_thread_idx; i < in_histogram_size; i += in_num_threads)
    {
      *gmem_bin_ptr = *histogram_bin_ptr;
      gmem_bin_ptr = make_step(gmem_bin_ptr, gmem_step, float);
      histogram_bin_ptr = make_step(histogram_bin_ptr, shared_step, float);
    }
  }
}

__global__ void computeBlockHistogramsWithGauss(cudaPitchedPtr inputImage,
                                                cudaPitchedPtr blockHistograms,
                                                int noHistogramBins,
                                                int cellSizeX, int cellSizeY,
                                                int blockSizeX, int blockSizeY)
{
  // number of HOG blocks per CUDA block
  int num_HOG_blocks_x = blockDim.x;
  int num_HOG_blocks_y = blockDim.y;
  int num_threads_per_histogram = blockDim.z;
  int num_cache_line_elems = (num_HOG_blocks_x + blockSizeX - 1) * cellSizeX;
  int num_cache_lines = (num_HOG_blocks_y + blockSizeY - 1);

	int i;

  // index of the HOG block along x direction
  int block_x_idx = threadIdx.x;
  // index of the HOG block along y direction
  int block_y_idx = threadIdx.y;
  // index of the cell to write results
  int cell_idx = threadIdx.z;

  // linear index of the HOG block in the CUDA block
  int block_lin_idx = block_y_idx * num_HOG_blocks_x + block_x_idx;

  // index of the first processed by the current CUDA block HOG block in the image
  int global_first_block_x_idx = blockIdx.x * num_HOG_blocks_x;
  int global_first_block_y_idx = blockIdx.y * num_HOG_blocks_y;
  // indices of the processed HOG block in the image
  int global_block_x_idx = global_first_block_x_idx + block_x_idx;
  int global_block_y_idx = global_first_block_y_idx + block_y_idx;

  // linear index of the thread in the block
  int thread_x_idx = threadIdx.x + threadIdx.z * blockDim.x;
  int thread_y_idx = threadIdx.y;
  // number of threads in the block
  int num_threads_x = blockDim.x * blockDim.z;
  int num_threads_y = blockDim.y;
  // number of left blocks to process
  int num_left_blocks = blockHistograms.xsize - global_first_block_x_idx;
  num_left_blocks = num_left_blocks > num_HOG_blocks_x ? num_HOG_blocks_x :
    num_left_blocks;
  // number of elements in row to process
  int left_elems_x = (num_left_blocks + blockSizeX - 1) * cellSizeX;
  // number of rows to process
  num_left_blocks = blockHistograms.ysize - global_first_block_y_idx;
  num_left_blocks = num_left_blocks > num_HOG_blocks_y ? num_HOG_blocks_y :
    num_left_blocks;
  int left_cells = num_left_blocks + blockSizeY - 1;

  // width of the HOG block
  int block_width = cellSizeX * blockSizeX;

  // size of the histogram for the current block
	int histogramSize = __mul24(noHistogramBins, blockSizeX) * blockSizeY;

  size_t img_col_base = global_first_block_x_idx * cellSizeX;

	float pIx, pIy;

	int fIy;
	int cIy;
  int Ix;
  float fx;
	float dy;
	float cy;

	bool lowervalidy;
	bool uppervalidy;

  // location in the shared memory of the histogram
  size_t img_row = __mul24(global_first_block_y_idx, cellSizeY);
  // location of the current element to read from the global memory
  float *gmemReadBase =
    get_elem(inputImage, img_row, img_col_base, float);
  // step between consecutive rows of cells in the input image
  int gmem_cell_y_step = inputImage.pitch * cellSizeY;
  int gmem_plane_step = inputImage.pitch * inputImage.ysize;

  cudaPitchedPtr gmem;
  gmem.ptr = gmemReadBase;
  gmem.pitch = gmem_cell_y_step;
  gmem.xsize = left_elems_x;
  gmem.ysize = left_cells;

  cudaPitchedPtr cache;
  cache.ptr = (float*)(allShared);
  cache.pitch = (num_cache_line_elems + 0) * sizeof(float);
  cache.xsize = num_cache_line_elems;
  cache.ysize = num_cache_lines;

  size_t cache_plane_step = cache.pitch * num_cache_lines;
  size_t cache_size = 2 * cache_plane_step;

  float* shLocalHistograms = (float*)((char*)allShared + cache_size);

  float *magnitude_base = get_elem(cache, block_y_idx, block_x_idx * cellSizeX, float);
  float *angle_base = make_step(magnitude_base, cache_plane_step, float);

  // location in the shared memory of the histogram
  float *histogram_ptr = shLocalHistograms + block_lin_idx;
  size_t histogram_step = num_HOG_blocks_x * num_HOG_blocks_y * sizeof(float);

  // initialize histograms
  for (i = 0; i < histogramSize; ++i)
    *make_step(histogram_ptr, i * histogram_step, float) = 0;

  float pIx_base = halfBin[0] - oneHalf + (0.5f - cenBound[0]) * bandWidth[0];
  float pIy_base = halfBin[1] - oneHalf + (0.5f - cenBound[1]) * bandWidth[1];

  int idx = 0;

  for (i=0; i < cellSizeY; i++)
  {
    // read data to the cache
    readData(gmem, gmem_plane_step, cache, cache_plane_step,
      thread_x_idx, thread_y_idx, num_threads_x, num_threads_y);
    gmem.ptr = make_step(gmem.ptr, inputImage.pitch, float);

    __syncthreads();

    // process data in the cache
    float *angle_ptr = angle_base;
    float *magnitude_ptr = magnitude_base;

    pIy = pIy_base + i * bandWidth[1];
    for (int cell_y_idx = 0; cell_y_idx < blockSizeY; ++cell_y_idx)
    {

      computeApproximations(pIy, fIy, cIy, dy, cy);
      lowervalidy = isValueInRange(fIy, blockSizeY);
      uppervalidy = isValueInRange(cIy, blockSizeY);

      pIx = pIx_base;

      // process all columns of the current row.
      for (int column_idx = 0; column_idx < block_width; ++column_idx)
      {
        if (cell_idx == 0)
        {
          computeFloorApproximations(pIx, Ix, fx);
          // lowervalidx
          bool canWrite = isValueInRange(Ix, blockSizeX);
          canWrite &= lowervalidy;
          if (canWrite)
          {
            float factor = fx * cy;
            int offset = (Ix + fIy * blockSizeY) * noHistogramBins;
            AddToHistogram(magnitude_ptr, angle_ptr, histogram_ptr,
              noHistogramBins, histogram_step, column_idx,
              idx, offset, factor);
          }
        }
        else if (cell_idx == 1)
        {
          computeFloorApproximations(pIx, Ix, fx);
          // lowervalidx
          bool canWrite = isValueInRange(Ix, blockSizeX);
          canWrite &= uppervalidy;
          if (canWrite)
          {
            float factor = fx * dy;
            int offset = (Ix + cIy * blockSizeY) * noHistogramBins;

            AddToHistogram(magnitude_ptr, angle_ptr, histogram_ptr,
              noHistogramBins, histogram_step, column_idx,
              idx, offset, factor);
          }
        }
        else if (cell_idx == 2)
        {
          computeCeilApproximations(pIx, Ix, fx);
          // uppervalidx
          bool canWrite = isValueInRange(Ix, blockSizeX);
          canWrite &= lowervalidy;
          if (canWrite)
          {
            float factor = fx * cy;
            int offset = (Ix + fIy * blockSizeY) * noHistogramBins;

            AddToHistogram(magnitude_ptr, angle_ptr, histogram_ptr,
              noHistogramBins, histogram_step, column_idx,
              idx, offset, factor);
          }
        }
        else if (cell_idx == 3)
        {
          computeCeilApproximations(pIx, Ix, fx);
          // uppervalidx
          bool canWrite = isValueInRange(Ix, blockSizeX);
          canWrite &= uppervalidy;
          if (canWrite)
          {
            float factor = fx * dy;
            int offset = (Ix + cIy * blockSizeY) * noHistogramBins;

            AddToHistogram(magnitude_ptr, angle_ptr, histogram_ptr,
              noHistogramBins, histogram_step, column_idx,
              idx, offset, factor);
          }
        }

        pIx += bandWidth[0];
        __syncthreads();
        ++idx;
      }
      magnitude_ptr = make_step(magnitude_ptr, cache.pitch, float);
      angle_ptr = make_step(angle_ptr, cache.pitch, float);
      pIy += cellSizeY * bandWidth[1];
    }
  }

  // write data to global memory
  writeData(blockHistograms, histogram_ptr, histogram_step, histogramSize,
    global_block_x_idx, global_block_y_idx, cell_idx, num_threads_per_histogram);
}

__global__ void computeBlockHistogramsWithGaussConstant(cudaPitchedPtr inputImage,
                                                cudaPitchedPtr blockHistograms,
                                                int noHistogramBins,
                                                int cellSizeX, int cellSizeY,
                                                int blockSizeX, int blockSizeY)
{
  // number of HOG blocks per CUDA block
  int num_HOG_blocks_x = blockDim.x;
  int num_HOG_blocks_y = blockDim.y;
  int num_threads_per_histogram = blockDim.z;
  int num_cache_line_elems = (num_HOG_blocks_x + blockSizeX - 1) * cellSizeX;
  int num_cache_lines = (num_HOG_blocks_y + blockSizeY - 1);

	int i;

  // index of the HOG block along x direction
  int block_x_idx = threadIdx.x;
  // index of the HOG block along y direction
  int block_y_idx = threadIdx.y;
  // index of the cell to write results
  int cell_idx = threadIdx.z;

  // linear index of the HOG block in the CUDA block
  int block_lin_idx = block_y_idx * num_HOG_blocks_x + block_x_idx;

  // linear index of the thread in the block
  int thread_x_idx = threadIdx.x + threadIdx.z * blockDim.x;
  int thread_y_idx = threadIdx.y;
  // number of threads in the block
  int num_threads_x = blockDim.x * blockDim.z;
  int num_threads_y = blockDim.y;

  // width of the HOG block
  int block_width = cellSizeX * blockSizeX;

  // size of the histogram for the current block
	int histogramSize = __mul24(noHistogramBins, blockSizeX) * blockSizeY;

	float pIx, pIy;

	int fIy;
	int cIy;
  int Ix;
  float fx;
	float dy;
	float cy;

	bool lowervalidy;
	bool uppervalidy;

  cudaPitchedPtr cache;
  cache.ptr = (float*)(allShared);
  cache.pitch = (num_cache_line_elems + 0) * sizeof(float);
  cache.xsize = num_cache_line_elems;
  cache.ysize = num_cache_lines;

  size_t cache_plane_step = cache.pitch * num_cache_lines;
  size_t cache_size = 2 * cache_plane_step;

  float* shLocalHistograms = (float*)((char*)allShared + cache_size);

  float *magnitude_base = get_elem(cache, block_y_idx, block_x_idx * cellSizeX, float);
  float *angle_base = make_step(magnitude_base, cache_plane_step, float);

  // location in the shared memory of the histogram
  float *histogram_ptr = shLocalHistograms + block_lin_idx;
  size_t histogram_step = num_HOG_blocks_x * num_HOG_blocks_y * sizeof(float);

  float pIx_base = halfBin[0] - oneHalf + (0.5f - cenBound[0]) * bandWidth[0];
  float pIy_base = halfBin[1] - oneHalf + (0.5f - cenBound[1]) * bandWidth[1];

  // index of the first processed by the current CUDA block HOG block in the image
  int global_first_block_x_idx = blockIdx.x * num_HOG_blocks_x;
  // indices of the processed HOG block in the image
  int global_block_x_idx = global_first_block_x_idx + block_x_idx;
  
  size_t img_col_base = global_first_block_x_idx * cellSizeX;

  // number of left blocks to process
  int num_left_blocks = blockHistograms.xsize - global_first_block_x_idx;
  num_left_blocks = num_left_blocks > num_HOG_blocks_x ? num_HOG_blocks_x :
    num_left_blocks;
  // number of elements in row to process
  int left_elems_x = (num_left_blocks + blockSizeX - 1) * cellSizeX;

  for (int iter = 0; iter < NROWS; ++iter)
  {
    // initialize histograms
    for (i = 0; i < histogramSize; ++i)
      *make_step(histogram_ptr, i * histogram_step, float) = 0;

    int global_first_block_y_idx = blockIdx.y * num_HOG_blocks_y + iter * num_HOG_blocks_y * gridDim.y;
    int global_block_y_idx = global_first_block_y_idx + block_y_idx;
    // number of rows to process
    num_left_blocks = blockHistograms.ysize - global_first_block_y_idx;
    num_left_blocks = num_left_blocks > num_HOG_blocks_y ? num_HOG_blocks_y :
      num_left_blocks;
    int left_cells = num_left_blocks + blockSizeY - 1;

    size_t img_row = __mul24(global_first_block_y_idx, cellSizeY);
    // location of the current element to read from the global memory
    float *gmemReadBase =
      get_elem(inputImage, img_row, img_col_base, float);
    // step between consecutive rows of cells in the input image
    int gmem_cell_y_step = inputImage.pitch * cellSizeY;
    int gmem_plane_step = inputImage.pitch * inputImage.ysize;

    cudaPitchedPtr gmem;
    gmem.ptr = gmemReadBase;
    gmem.pitch = gmem_cell_y_step;
    gmem.xsize = left_elems_x;
    gmem.ysize = left_cells;

    int idx = 0;
    for (i=0; i < cellSizeY; i++)
    {
      // read data to the cache
      readData(gmem, gmem_plane_step, cache, cache_plane_step,
        thread_x_idx, thread_y_idx, num_threads_x, num_threads_y);
      gmem.ptr = make_step(gmem.ptr, inputImage.pitch, float);

      __syncthreads();

      // process data in the cache
      float *angle_ptr = angle_base;
      float *magnitude_ptr = magnitude_base;

      pIy = pIy_base + i * bandWidth[1];
      for (int cell_y_idx = 0; cell_y_idx < blockSizeY; ++cell_y_idx)
      {
        computeApproximations(pIy, fIy, cIy, dy, cy);
        lowervalidy = isValueInRange(fIy, blockSizeY);
        uppervalidy = isValueInRange(cIy, blockSizeY);

        pIx = pIx_base;

        // process all columns of the current row.
        for (int column_idx = 0; column_idx < block_width; ++column_idx)
        {
          if (cell_idx == 0)
          {
            computeFloorApproximations(pIx, Ix, fx);
            // lowervalidx
            bool canWrite = isValueInRange(Ix, blockSizeX);
            canWrite &= lowervalidy;
            if (canWrite)
            {
              float factor = fx * cy;
              int offset = (Ix + fIy * blockSizeY) * noHistogramBins;
              AddToHistogramConstant(magnitude_ptr, angle_ptr, histogram_ptr,
                noHistogramBins, histogram_step, column_idx, idx,
                offset, factor);
            }
          }
          else if (cell_idx == 1)
          {
            computeFloorApproximations(pIx, Ix, fx);
            // lowervalidx
            bool canWrite = isValueInRange(Ix, blockSizeX);
            canWrite &= uppervalidy;
            if (canWrite)
            {
              float factor = fx * dy;
              int offset = (Ix + cIy * blockSizeY) * noHistogramBins;

              AddToHistogramConstant(magnitude_ptr, angle_ptr, histogram_ptr,
                noHistogramBins, histogram_step, column_idx, idx,
                offset, factor);
            }
          }
          else if (cell_idx == 2)
          {
            computeCeilApproximations(pIx, Ix, fx);
            // uppervalidx
            bool canWrite = isValueInRange(Ix, blockSizeX);
            canWrite &= lowervalidy;
            if (canWrite)
            {
              float factor = fx * cy;
              int offset = (Ix + fIy * blockSizeY) * noHistogramBins;

              AddToHistogramConstant(magnitude_ptr, angle_ptr, histogram_ptr,
                noHistogramBins, histogram_step, column_idx, idx,
                offset, factor);
            }
          }
          else if (cell_idx == 3)
          {
            computeCeilApproximations(pIx, Ix, fx);
            // uppervalidx
            bool canWrite = isValueInRange(Ix, blockSizeX);
            canWrite &= uppervalidy;
            if (canWrite)
            {
              float factor = fx * dy;
              int offset = (Ix + cIy * blockSizeY) * noHistogramBins;

              AddToHistogramConstant(magnitude_ptr, angle_ptr, histogram_ptr,
                noHistogramBins, histogram_step, column_idx, idx,
                offset, factor);
            }
          }

          pIx += bandWidth[0];
          __syncthreads();
          ++idx;
        }
        magnitude_ptr = make_step(magnitude_ptr, cache.pitch, float);
        angle_ptr = make_step(angle_ptr, cache.pitch, float);
        pIy += cellSizeY * bandWidth[1];
      }
    }

    // write data to global memory
    writeData(blockHistograms, histogram_ptr, histogram_step, histogramSize,
      global_block_x_idx, global_block_y_idx, cell_idx, num_threads_per_histogram);
  }
}

__host__ void HOGHistogram::ComputeBlockHistogramsWithGauss(
  cudaPitchedPtr inputImage, cudaPitchedPtr blockHistograms, int noHistogramBins,
  int cellSizeX, int cellSizeY, int blockSizeX, int blockSizeY,
  int windowSizeX, int windowSizeY,
  int width, int height, int &out_num_blocks_x, int &out_num_blocks_y,
  int &out_num_windows_x, int &out_num_windows_y)
{
  if (!m_is_set)
    exit(-1);

	int leftoverX;
	int leftoverY;

	dim3 hThreadSize, hBlockSize;

	int rNoOfCellsX = width / cellSizeX;
	int rNoOfCellsY = height / cellSizeY;

  int num_blocks_x = 16;
  int num_blocks_y = 4;
	out_num_blocks_x = rNoOfCellsX - blockSizeX + 1;
	out_num_blocks_y = rNoOfCellsY - blockSizeY + 1;

	out_num_windows_x = (width-windowSizeX)/cellSizeX + 1;
	out_num_windows_y = (height-windowSizeY)/cellSizeY + 1;

	leftoverX = (width - windowSizeX - cellSizeX * (out_num_windows_x - 1))/2;
	leftoverY = (height - windowSizeY - cellSizeY * (out_num_windows_y - 1))/2;

	hThreadSize = dim3(num_blocks_x, num_blocks_y, 4);
	hBlockSize = dim3((out_num_blocks_x - 1) / num_blocks_x + 1,
    (out_num_blocks_y - 1) / num_blocks_y + 1);
  hBlockSize.y = (hBlockSize.y - 1) / NROWS + 1;

  size_t shared_histogram = num_blocks_y * num_blocks_x *
    noHistogramBins * blockSizeX * blockSizeY * sizeof(float);
  size_t shared_cache = 2 * (num_blocks_y + blockSizeY - 1) *
    ((num_blocks_x + blockSizeX - 1) * cellSizeX + 0) * sizeof(float);
  size_t shared_size = shared_cache + shared_histogram;

  inputImage.ptr = get_elem(inputImage, leftoverY, leftoverX, float);
  inputImage.xsize = width;
  inputImage.ysize = height;

  blockHistograms.xsize = out_num_blocks_x;
  blockHistograms.ysize = out_num_blocks_y;

  if (cellSizeX * cellSizeY * blockSizeX * blockSizeY <= MAX_CONSTANT)
  {
    computeBlockHistogramsWithGaussConstant<<<hBlockSize, hThreadSize, shared_size, m_stream>>>
      (inputImage, blockHistograms, noHistogramBins, cellSizeX, cellSizeY,
      blockSizeX, blockSizeY);
  }
  else
  {
  	computeBlockHistogramsWithGauss<<<hBlockSize, hThreadSize, shared_size, m_stream>>>
		  (inputImage, blockHistograms, noHistogramBins, cellSizeX, cellSizeY,
      blockSizeX, blockSizeY);
  }
}


__global__ void normalizeBlockHistograms(cudaPitchedPtr blockHistograms,
                                         int in_histogram_size,
                                         int rNoOfHOGBlocksX, int rNoOfHOGBlocksY,
                                         int alignedBlockSize)
{
  int bin_idx, threads_per_histogram;
  int block_x_idx, block_y_idx, global_block_x_idx, global_block_y_idx;
  int num_hog_blocks_per_block_x, num_hog_blocks_per_block_y;

  block_x_idx = threadIdx.x;
  block_y_idx = threadIdx.y;

  num_hog_blocks_per_block_x = blockDim.x;
  num_hog_blocks_per_block_y = blockDim.y;

  global_block_x_idx = block_x_idx + blockIdx.x * num_hog_blocks_per_block_x;
  global_block_y_idx = block_y_idx + blockIdx.y * num_hog_blocks_per_block_y;

  bin_idx = threadIdx.z;
  threads_per_histogram = blockDim.z;

  bool is_valid = (global_block_x_idx < rNoOfHOGBlocksX) &&
    (global_block_y_idx < rNoOfHOGBlocksY);

  int local_block_idx = block_x_idx + block_y_idx * num_hog_blocks_per_block_x;
  float* cache = (float*)allShared + local_block_idx * threads_per_histogram;
  float* all_histograms = (float*)allShared +
    threads_per_histogram * num_hog_blocks_per_block_x * num_hog_blocks_per_block_y;
  float* smem_start_ptr = (float*)all_histograms + local_block_idx;
  size_t smem_plane_step = num_hog_blocks_per_block_x * num_hog_blocks_per_block_y * sizeof(float);
  float* smem_bin_ptr;
  smem_start_ptr = make_step(smem_start_ptr, bin_idx * smem_plane_step, float);
  size_t smem_thread_step = threads_per_histogram * smem_plane_step;

  float norm1, norm2; float eps2 = 0.01f;

  size_t gmem_plane_step = blockHistograms.pitch * rNoOfHOGBlocksY;
  float *gmem_start_ptr =
    get_elem(blockHistograms, global_block_y_idx, global_block_x_idx, float);
  gmem_start_ptr = make_step(gmem_start_ptr, bin_idx * gmem_plane_step, float);
  size_t gmem_thread_step = threads_per_histogram * gmem_plane_step;
  float *gmem_bin_ptr;

  if (is_valid)
  {
    norm1 = 0;
    gmem_bin_ptr = gmem_start_ptr;
    smem_bin_ptr = smem_start_ptr;
    for (int idx = bin_idx; idx < in_histogram_size; idx += threads_per_histogram)
    {
      *smem_bin_ptr = *gmem_bin_ptr;
      norm1 += *smem_bin_ptr * *smem_bin_ptr;
      gmem_bin_ptr = make_step(gmem_bin_ptr, gmem_thread_step, float);
      smem_bin_ptr = make_step(smem_bin_ptr, smem_thread_step, float);
    }

    cache[bin_idx] = norm1;
  }

  __syncthreads();
  for (unsigned int s = alignedBlockSize >> 1; s > 0; s >>= 1)
  {
    if (is_valid && (bin_idx < s) && (bin_idx + s < threads_per_histogram))
      cache[bin_idx] += cache[bin_idx + s];
    __syncthreads();
  }

  if (is_valid)
  {
    norm1 = sqrtf(cache[0]) + in_histogram_size;

    norm2 = 0;
    smem_bin_ptr = smem_start_ptr;
    for (int idx = bin_idx; idx < in_histogram_size; idx += threads_per_histogram)
    {
      *smem_bin_ptr /= norm1;
      *smem_bin_ptr = fminf(0.2f, *smem_bin_ptr);
      norm2 += *smem_bin_ptr * *smem_bin_ptr;
      smem_bin_ptr = make_step(smem_bin_ptr, smem_thread_step, float);
    }

    cache[bin_idx] = norm2;
  }

  __syncthreads();
  for (unsigned int s = alignedBlockSize >> 1; s > 0; s >>= 1)
  {
    if (is_valid && (bin_idx < s) && (bin_idx + s < threads_per_histogram))
      cache[bin_idx] += cache[bin_idx + s];
    __syncthreads();
  }

  if (is_valid)
  {
    norm2 = sqrtf(cache[0]) + eps2;
    gmem_bin_ptr = gmem_start_ptr;
    smem_bin_ptr = smem_start_ptr;
    for (int idx = bin_idx; idx < in_histogram_size; idx += threads_per_histogram)
    {
      *gmem_bin_ptr = *smem_bin_ptr / norm2;
      gmem_bin_ptr = make_step(gmem_bin_ptr, gmem_thread_step, float);
      smem_bin_ptr = make_step(smem_bin_ptr, smem_thread_step, float);
    }
  }
}

__host__ void HOGHistogram::NormalizeBlockHistograms(cudaPitchedPtr blockHistograms,
  int in_histogram_size, int in_num_blocks_x, int in_num_blocks_y)
{
	dim3 hThreadSize, hBlockSize;

  int num_blocks_x = 32;
  int num_blocks_y = 2;
  int num_threads_per_histogram = 8;

  int hog_blocks_per_block = num_blocks_x * num_blocks_y;
	hThreadSize = dim3(num_blocks_x, num_blocks_y, num_threads_per_histogram);
  int alignedBlockSize = iClosestPowerOfTwo(num_threads_per_histogram);
  size_t shared_size = (num_threads_per_histogram + in_histogram_size) *
    hog_blocks_per_block * sizeof(float);

  hBlockSize = dim3((in_num_blocks_x - 1) / num_blocks_x + 1,
    (in_num_blocks_y - 1) / num_blocks_y + 1);

	normalizeBlockHistograms<<<hBlockSize, hThreadSize, shared_size, m_stream>>>
    (blockHistograms, in_histogram_size, in_num_blocks_x, in_num_blocks_y,
    alignedBlockSize);
}

__host__ void HOGHistogram::CountNumBlocks(int in_width, int in_height,
                                           int in_cell_size_x, int in_cell_size_y, 
                                           int in_block_size_x, int in_block_size_y,
                                           int &out_num_blocks_x, int &out_num_blocks_y)
{
	int num_cells_x = in_width / in_cell_size_x;
	int num_cells_y = in_height / in_cell_size_y;

  out_num_blocks_x = num_cells_x - in_block_size_x + 1;
  out_num_blocks_y = num_cells_y - in_block_size_y + 1;
}
