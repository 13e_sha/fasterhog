#include "HOGScale.h"
#include "HOGUtils.h"

texture<float4, 2, cudaReadModeElementType> tex;

bool HOGScale::m_is_alocated[] = {false, false};
cudaArray *HOGScale::imageArray[];// = 0;
cudaChannelFormatDesc HOGScale::channelDescDownscale;
bool HOGScale::m_is_bound = false;

// w0, w1, w2, and w3 are the four cubic B-spline basis functions
__device__ float w0(float a) { return (1.0f/6.0f)*(a*(a*(-a + 3.0f) - 3.0f) + 1.0f); }
__device__ float w1(float a) { return (1.0f/6.0f)*(a*a*(3.0f*a - 6.0f) + 4.0f); }
__device__ float w2(float a) { return (1.0f/6.0f)*(a*(a*(-3.0f*a + 3.0f) + 3.0f) + 1.0f); }
__device__ float w3(float a) { return (1.0f/6.0f)*(a*a*a); }

// g0 and g1 are the two amplitude functions
__device__ float g0(float a) { return w0(a) + w1(a); }
__device__ float g1(float a) { return w2(a) + w3(a); }

// h0 and h1 are the two offset functions
__device__ float h0(float a) { return -1.0f + w1(a) / (w0(a) + w1(a)) + 0.5f; }
__device__ float h1(float a) { return 1.0f + w3(a) / (w2(a) + w3(a)) + 0.5f; }

__global__ void resizeFastBicubic4(cudaPitchedPtr outputFloat, float4* paddedRegisteredImage, int width, int height, float scale);
__global__ void resizeFastBicubic1(cudaPitchedPtr outputFloat, float4* paddedRegisteredImage, int width, int height, float scale);

__host__ void HOGScale::InitScale(int in_width, int in_height, int in_idx)
{
  CloseScale(in_idx);

  channelDescDownscale = cudaCreateChannelDesc<float4>();
	tex.filterMode = cudaFilterModeLinear;
	tex.normalized = false;

  cutilSafeCall(cudaMallocArray(&imageArray[in_idx], &channelDescDownscale,
    in_width, in_height));
  m_is_alocated[in_idx] = true;
}

__host__ void HOGScale::CloseScale(int in_idx)
{
  ReleaseImage();
  if (m_is_alocated[in_idx])
  {
    cutilSafeCall(cudaFreeArray(imageArray[in_idx]));
    m_is_alocated[in_idx] = false;
  }
}

__host__ void HOGScale::SetImage(float4 *in_image, int in_width, int in_height,
                                 int in_idx, cudaStream_t in_stream)
{
  ReleaseImage();
  cutilSafeCall(cudaMemcpyToArrayAsync(imageArray[in_idx], 0, 0, in_image,
    sizeof(float4) * in_width * in_height, cudaMemcpyDeviceToDevice, in_stream));

	cutilSafeCall(cudaBindTextureToArray(tex, imageArray[in_idx],
    channelDescDownscale));
  m_is_bound = true;
}

__host__ void HOGScale::ReleaseImage()
{
  if (m_is_bound)
  {
    cutilSafeCall(cudaUnbindTexture(tex));
    m_is_bound = false;
  }
}

__host__ void HOGScale::GetResizedSize(int in_width, int in_height,
  float in_scale, int &out_width, int &out_height)
{
  out_width = iDivUpF(in_width, in_scale);
  out_height = iDivUpF(in_height, in_scale);
}

__host__ HOGScale::HOGScale(int in_width, int in_height, cudaStream_t in_stream)
  : m_stream(in_stream)
{
  m_width = in_width;
  m_height = in_height;
}

__host__ HOGScale::~HOGScale()
{
}

__host__ void HOGScale::DownscaleImage(float scale, bool useGrayscale,
                                       float4* paddedRegisteredImage,
                                       cudaPitchedPtr resizedPaddedImage,
                                       int& out_width, int &out_height)
{
  if (!m_is_bound)
    exit(-1);

	dim3 hThreadSize, hBlockSize;

	hThreadSize = dim3(THREAD_SIZE_W, THREAD_SIZE_H);

  GetResizedSize(m_width, m_height, scale, out_width, out_height);

  hBlockSize =
    dim3(iDivUp(out_width, hThreadSize.x), iDivUp(out_height, hThreadSize.y));

	if (useGrayscale)
  {
    resizeFastBicubic1<<<hBlockSize, hThreadSize, 0, m_stream>>>(
      resizedPaddedImage, paddedRegisteredImage, out_width, out_height, scale);
  }
	else
  {
    resizeFastBicubic4<<<hBlockSize, hThreadSize, 0, m_stream>>>(
      resizedPaddedImage, paddedRegisteredImage, out_width, out_height, scale);
  }
}

__device__ float4 tex2DFastBicubic(const texture<float4, 2, cudaReadModeElementType> texref, float x, float y)
{
	float4 r;
	float4 val0, val1, val2, val3;

	x -= 0.5f;
	y -= 0.5f;
	float px = floor(x);
	float py = floor(y);
	float fx = x - px;
	float fy = y - py;

	float g0x = g0(fx);
	float g1x = g1(fx);
	float h0x = h0(fx);
	float h1x = h1(fx);
	float h0y = h0(fy);
	float h1y = h1(fy);

	val0 = tex2D(texref, px + h0x, py + h0y);
	val1 = tex2D(texref, px + h1x, py + h0y);
	val2 = tex2D(texref, px + h0x, py + h1y);
	val3 = tex2D(texref, px + h1x, py + h1y);

	r.x = (g0(fy) * (g0x * val0.x + g1x * val1.x) + g1(fy) * (g0x * val2.x + g1x * val3.x));
	r.y = (g0(fy) * (g0x * val0.y + g1x * val1.y) + g1(fy) * (g0x * val2.y + g1x * val3.y));
	r.z = (g0(fy) * (g0x * val0.z + g1x * val1.z) + g1(fy) * (g0x * val2.z + g1x * val3.z));
	r.w = (g0(fy) * (g0x * val0.w + g1x * val1.w) + g1(fy) * (g0x * val2.w + g1x * val3.w));

	return r;
}

__global__ void resizeFastBicubic4(cudaPitchedPtr outputFloat, float4* paddedRegisteredImage, int width, int height, float scale)
{
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
	int i = __umul24(y, width) + x;

  size_t plane_step = height * outputFloat.pitch;

	float u = x*scale;
	float v = y*scale;

	if (x < width && y < height)
	{
		float4 cF;

		if (scale == 1.0f)
		{
			cF = paddedRegisteredImage[i];
			cF.w = 0;
		}
		else
		{
			cF = tex2D(tex, u, v);
			cF.w = 0;
		}

    float *out_ptr = get_elem(outputFloat, y, x, float);
    *out_ptr = sqrtf(cF.x);
    *(out_ptr = make_step(out_ptr, plane_step, float)) = sqrt(cF.y);
    *make_step(out_ptr, plane_step, float) = sqrt(cF.z);
	}
}

__global__ void resizeFastBicubic1(cudaPitchedPtr outputFloat, float4* paddedRegisteredImage, int width, int height, float scale)
{
	int x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	int y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
	int i = __umul24(y, width) + x;

	float u = x*scale;
	float v = y*scale;

	if (x < width && y < height)
	{
		float4 cF;

		if (scale == 1.0f)
		{
			cF = paddedRegisteredImage[i];
			cF.w = 0;
		}
		else
		{
			cF = tex2D(tex, u, v);
			cF.w = 0;
		}

		*get_elem(outputFloat, y, x, float) = sqrtf(0.2989f * cF.x + 0.5870f * cF.y + 0.1140f * cF.z);
	}
}
