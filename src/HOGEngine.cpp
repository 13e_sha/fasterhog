#include "HOGEngine.h"
#include "HOGNMS.h"

#include "HOGDefines.h"

#include <stdlib.h>
#include <math.h>
#include <string.h>

using namespace HOG;

HOGEngine* HOGEngine::instance;

int HOGEngine::iDivUpF(int a, float b) { return (a % int(b) != 0) ? int(a / b + 1) : int(a / b);}

void HOGEngine::InitializeHOG(int iw, int ih, std::string fileName)
{
	this->imageWidth = iw;
	this->imageHeight = ih;

	this->avSizeX = 0;
	this->avSizeY = 0;
	this->marginX = 0;
	this->marginY = 0;

	this->hCellSizeX = 4; // 8
	this->hCellSizeY = 4; // 8
	this->hBlockSizeX = 2;
	this->hBlockSizeY = 2;
	this->hWindowSizeX = 24; //64
	this->hWindowSizeY = 24; //128
	this->hNoOfHistogramBins = 9;

	this->wtScale = 2.0f;

	this->useGrayscale = false;

	this->readSVMFromFile(fileName);

	this->formattedResultsAvailable = false;

	nmsProcessor = new HOGNMS();

	m_engine.InitHOG(iw, ih, avSizeX, avSizeY, marginX, marginY, hCellSizeX, hCellSizeY,
    hBlockSizeX, hBlockSizeY, hWindowSizeX, hWindowSizeY, hNoOfHistogramBins, wtScale,
    svmBias, svmWeights, svmWeightsCount, useGrayscale);
}

void HOGEngine::InitializeHOG(int iw, int ih, float svmBias, float* svmWeights, int svmWeightsCount)
{
	this->imageWidth = iw;
	this->imageHeight = ih;

	this->avSizeX = 48; //48
	this->avSizeY = 96; //96
	this->marginX = 4; // 4
	this->marginY = 4; // 4

	this->hCellSizeX = 8;
	this->hCellSizeY = 8;
	this->hBlockSizeX = 2;
	this->hBlockSizeY = 2;
	this->hWindowSizeX = 64;
	this->hWindowSizeY = 128;
	this->hNoOfHistogramBins = 9;

	this->svmWeightsCount = svmWeightsCount;
	this->svmBias = svmBias;
	this->svmWeights = svmWeights;

	this->wtScale = 2.0f;

	this->useGrayscale = false;

	this->formattedResultsAvailable = false;

	nmsProcessor = new HOGNMS();

	m_engine.InitHOG(iw, ih, avSizeX, avSizeY, marginX, marginY, hCellSizeX, hCellSizeY,
    hBlockSizeX, hBlockSizeY, hWindowSizeX, hWindowSizeY, hNoOfHistogramBins, wtScale,
    svmBias, svmWeights, svmWeightsCount, useGrayscale);
}


void HOGEngine::readSVMFromFile(std::string modelfile)
{
	double linearbias_, *linearwt_;

    FILE *modelfl;
#ifdef _WIN32
    if ((fopen_s (&modelfl, modelfile.c_str(), "rb")) != 0)
    { printf("File not found!\n"); exit(1); }
#else
    if ((modelfl = fopen (modelfile.c_str(), "rb")) == NULL)
    { printf("File not found!\n"); exit(1); }
#endif
    char version_buffer[10];
    if (!fread (&version_buffer,sizeof(char),10,modelfl))
    { printf("Wrong file version!\n"); exit(1); }

    if(strcmp(version_buffer,"V6.01")) {
    	printf("Wrong file version!\n"); exit(1);
    }
    /* read version number */
    int version = 0;
    if (!fread (&version,sizeof(int),1,modelfl))
    { printf("Wrong file version!\n"); exit(1); }
    if (version < 200)
    { printf("Wrong file version!\n"); exit(1); }

    long long kernel_type;
    if (!fread(&(kernel_type),sizeof(long long),1,modelfl))
    { printf("Cannot read kernel type!\n"); exit(1); }

    {// ignore these
        long long poly_degree;
        if (!fread(&(poly_degree),sizeof(long long),1,modelfl))
        { printf("Cannot read degree of polynomial!\n"); exit(1); }

        double rbf_gamma;
        if (!fread(&(rbf_gamma),sizeof(double),1,modelfl))
        { printf("Cannot read gamma value!\n"); exit(1); }

        double  coef_lin;
        if (!fread(&(coef_lin),sizeof(double),1,modelfl))
        { printf("Cannot read linear coefficient!\n"); exit(1); }
        double coef_const;
        if (!fread(&(coef_const),sizeof(double),1,modelfl))
        { printf("Cannot read constant coefficient!\n"); exit(1); }

        long long l;
        if (!fread(&l,sizeof(long long),1,modelfl))
        { printf("Cannot read number of parameters!\n"); exit(1); }
        char* custom = new char[(unsigned int)l];
        if (fread(custom,sizeof(char),(size_t)l,modelfl) != (size_t)l)
        { printf("Cannot read custom parameters!\n"); exit(1); }
        delete[] custom;
    }

    long long totwords;
    if (!fread(&(totwords),sizeof(long long),1,modelfl))
    { printf("Cannot read number of linear parameters!\n"); exit(1); }

    {// ignore these
        long long totdoc;
        if (!fread(&(totdoc),sizeof(long long),1,modelfl))
        { printf("Cannot read totdoc parameter!\n"); exit(1); }

        long long sv_num;
        if (!fread(&(sv_num), sizeof(long long),1,modelfl))
        { printf("Cannot read sv_num parameter!\n"); exit(1); }
    }

    if (!fread(&linearbias_, sizeof(double),1,modelfl))
    { printf("Cannot read bias parameter!\n"); exit(1); }

    if(kernel_type == 0) { /* linear kernel */
        /* save linear wts also */
        linearwt_ = new double[(unsigned int)totwords+1];
		svmWeightsCount = (int) totwords;
        if (fread(linearwt_, sizeof(double),(size_t)totwords+1,modelfl) != (size_t)(totwords + 1))
        { printf("Cannot read linear svm parameters!\n"); exit(1); }
    } else {
        exit(1);
    }

	svmWeights = new float[svmWeightsCount+1];
	for (int i=0; i<svmWeightsCount; i++)
		svmWeights[i] = (float) linearwt_[i];

	svmBias = (float)linearbias_;

	fclose(modelfl);

	delete linearwt_;
}

void HOGEngine::FinalizeHOG()
{
	delete nmsProcessor;

	m_engine.CloseHOG();
}

void HOGEngine::BeginProcess(HOGImage* hostImage,
		int _minx, int _miny, int _maxx, int _maxy, float minScale, float maxScale)
{
	minX = _minx, minY = _miny, maxX = _maxx, maxY = _maxy;

	if (minY == -1 && minY == -1 && maxX == -1 && maxY == -1)
	{
		minX = 0;
		minY = 0;
		maxX = imageWidth;
		maxY = imageHeight;
	}

	m_engine.BeginHOGProcessing(hostImage->pixels, minX, minY, maxX, maxY, minScale, maxScale);
}

void HOGEngine::EndProcess()
{
	ResponsesAdapter &adapter = m_engine.EndHOGProcessing();

	m_engine.GetHOGParameters(&startScale, &endScale, &scaleRatio, &scaleCount,
		&hPaddingSizeX, &hPaddingSizeY, &hPaddedWidth, &hPaddedHeight,
		&hNoOfCellsX, &hNoOfCellsY, &hNoOfBlocksX, &hNoOfBlocksY, &hNumberOfWindowsX,
		&hNumberOfWindowsY, &hNumberOfBlockPerWindowX, &hNumberOfBlockPerWindowY);

  ComputeFormattedResults(adapter);

	nmsResults = nmsProcessor->ComputeNMSResults(formattedResults, formattedResultsCount, &nmsResultsAvailable, &nmsResultsCount,
		hWindowSizeX, hWindowSizeY);
}

void HOGEngine::ContinueProcess(HOGImage* in_image, int in_min_x, int in_min_y,
                                int in_max_x, int in_max_y,
                                float in_min_scale, float in_max_scale)
{
	minX = in_min_x, minY = in_min_y, maxX = in_max_x, maxY = in_max_y;

	if (minY == -1 && minY == -1 && maxX == -1 && maxY == -1)
	{
		minX = 0;
		minY = 0;
		maxX = imageWidth;
		maxY = imageHeight;
	}

	ResponsesAdapter &adapter = m_engine.ContinueHOGProcessing(in_image->pixels, minX, minY, maxX, maxY,
    in_min_scale, in_max_scale);

	m_engine.GetHOGParameters(&startScale, &endScale, &scaleRatio, &scaleCount,
		&hPaddingSizeX, &hPaddingSizeY, &hPaddedWidth, &hPaddedHeight,
		&hNoOfCellsX, &hNoOfCellsY, &hNoOfBlocksX, &hNoOfBlocksY, &hNumberOfWindowsX,
		&hNumberOfWindowsY, &hNumberOfBlockPerWindowX, &hNumberOfBlockPerWindowY);

  ComputeFormattedResults(adapter);

	nmsResults = nmsProcessor->ComputeNMSResults(formattedResults, formattedResultsCount, &nmsResultsAvailable, &nmsResultsCount,
		hWindowSizeX, hWindowSizeY);
}


void HOGEngine::SaveResultsToDisk(char* fileName)
{
	FILE* f; 
#ifdef _WIN32
	fopen_s(&f, fileName, "w+");
#else
	f = fopen(fileName, "w+");
#endif
	fprintf(f, "%d\n", formattedResultsCount);
	for (int i=0; i<formattedResultsCount; i++)
	{
		fprintf(f, "%f %f %d %d %d %d %d %d\n",
			formattedResults[i].scale, formattedResults[i].score,
			formattedResults[i].width, formattedResults[i].height,
			formattedResults[i].x, formattedResults[i].y,
			formattedResults[i].origX, formattedResults[i].origY);
	}
	fclose(f);
}

void HOGEngine::ComputeFormattedResults(ResponsesAdapter &in_adapter)
{
	int i, resultId;
  unsigned int j, k;

	resultId = 0;
	formattedResultsCount = 0;

	float currentScale = startScale;

	for (i=0; i<scaleCount; i++)
	{
    cudaPitchedPtr map = in_adapter.GetLevel(i);
    float *row = (float*)map.ptr;
		for (j = 0; j < map.ysize; j++)
		{
			for (k = 0; k < map.xsize; k++)
			{
				float score = row[k];
				if (score > 0)
					formattedResultsCount++;
			}
      row = make_step(row, map.pitch, float);
		}
	}

	if (formattedResultsAvailable) delete formattedResults;
	formattedResults = new HOGResult[formattedResultsCount];

	for (i=0; i<scaleCount; i++)
	{
    cudaPitchedPtr map = in_adapter.GetLevel(i);
    float *row = (float*)map.ptr;
		for (j=0; j<map.ysize; j++)
		{
			for (k=0; k<map.xsize; k++)
			{
				float score = row[k];
				if (score > 0)
				{
					formattedResults[resultId] = map2Bbox(j, k, currentScale);
					formattedResults[resultId].score = score;
					resultId++;
				}
			}
      row = make_step(row, map.pitch, float);
		}
		currentScale = currentScale * scaleRatio;
	}
}

void map2Bbox1d(int in_x, float in_scale, int in_line_size, int in_cell_size,
    int in_window_size, int in_shift, int &out_orig, int &out_size, int &out_x)
{
  int current_line_size = iDivUpF(in_line_size, in_scale);
  int num_windows = (current_line_size - in_window_size) / in_cell_size + 1;
  int left_over = (current_line_size - in_window_size - in_cell_size * (num_windows - 1)) / 2;
  out_orig = in_x * in_cell_size + left_over;
  out_size = (int)floorf((float)in_window_size * in_scale);
  int delta = in_window_size / 2 - (float) in_window_size / 2;
  out_x = (int)ceilf(in_scale * (out_orig + delta)) + in_shift;
}

HOGResult HOGEngine::map2Bbox(int in_row, int in_col, float in_scale)
{
	HOGResult result;

  map2Bbox1d(in_col, in_scale, hPaddedWidth, hCellSizeX, hWindowSizeX,
      minX - hPaddingSizeX, result.origX, result.width, result.x);
  map2Bbox1d(in_row, in_scale, hPaddedHeight, hCellSizeY, hWindowSizeY,
      minY - hPaddingSizeY, result.origY, result.height, result.y);

	result.scale = in_scale;
	return result;
}
