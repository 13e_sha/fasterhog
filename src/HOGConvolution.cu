#include "HOGConvolution.h"
#include "HOGUtils.h"

#include <cassert>
#include <fstream>
#include <iostream>

unsigned int HOGConvolution::m_num_objects = 0;

#define convKernelRadius 1
#define convKernelWidth (2 * convKernelRadius + 1)
__device__ __constant__ float d_Kernel[convKernelWidth];

#define convRowTileWidth 128
#define convKernelRadiusAligned 16

#define convColumnTileWidth 16
#define convColumnTileHeight 8

const int convKernelSize = convKernelWidth * sizeof(float);

template<int i> __device__ float convolutionRow(float *data) {
	float val = data[convKernelRadius-i];
	val *= d_Kernel[i];
	val += convolutionRow<i-1>(data);
	return val;
}
template<> __device__ float convolutionRow<-1>(float *data){return 0;}
template<int i> __device__ float convolutionColumn(float *data) {
	float val = data[(convKernelRadius-i)*convColumnTileWidth];
	val *= d_Kernel[i];
	val += convolutionColumn<i-1>(data);
	return val;
}
template<> __device__ float convolutionColumn<-1>(float *data){return 0;}

__global__ void convolutionRowGPU1(cudaPitchedPtr d_Result, cudaPitchedPtr d_Data, int dataW, int dataH)
{
  const int row_idx = blockIdx.y;
	const int rowStart = IMUL(row_idx, dataW);

	__shared__ float data[convKernelRadius + convRowTileWidth + convKernelRadius];

	const int tileStart = IMUL(blockIdx.x, convRowTileWidth);
	const int tileEnd = tileStart + convRowTileWidth - 1;
	const int apronStart = tileStart - convKernelRadius;
	const int apronEnd = tileEnd + convKernelRadius;

	const int tileEndClamped = min(tileEnd, dataW - 1);
	const int apronStartClamped = max(apronStart, 0);
	const int apronEndClamped = min(apronEnd, dataW - 1);

	const int apronStartAligned = tileStart - convKernelRadiusAligned;

	const int loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
		const int smemPos = loadPos - apronStart;
		data[smemPos] =
      ((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
      *get_elem(d_Data, row_idx, loadPos, float) : 0.f;
	}

	__syncthreads();
	const int writePos = tileStart + threadIdx.x;

	if(writePos <= tileEndClamped)
	{
		const int smemPos = writePos - apronStart;
		float sum = convolutionRow<2 * convKernelRadius>(data + smemPos);
		*get_elem(d_Result, row_idx, writePos, float) = sum;
	}
}

__global__ void convolutionRowGPU4(cudaPitchedPtr d_Result, cudaPitchedPtr d_Data, int dataW, int dataH)
{
  float *gmem_ptr, *shmem_ptr;
  const int row_idx = blockIdx.y;
	const int rowStart = IMUL(row_idx, dataW);

  const int num_elems = convKernelRadius + convRowTileWidth + convKernelRadius;
  const int in_gmem_plane_step = dataH * d_Data.pitch;
  const int out_gmem_plane_step = dataH * d_Result.pitch;
  const int shmem_pitch = num_elems * sizeof(float);
  __shared__ float data[3 * num_elems];

	const int tileStart = IMUL(blockIdx.x, convRowTileWidth);
	const int tileEnd = tileStart + convRowTileWidth - 1;
	const int apronStart = tileStart - convKernelRadius;
	const int apronEnd = tileEnd + convKernelRadius;

	const int tileEndClamped = min(tileEnd, dataW - 1);
	const int apronStartClamped = max(apronStart, 0);
	const int apronEndClamped = min(apronEnd, dataW - 1);

	const int apronStartAligned = tileStart - convKernelRadiusAligned;

  // position of the last element in the global memory to read
  const int last_pos = num_elems + apronStart;

  for (int loadPos = apronStartAligned + threadIdx.x; loadPos < last_pos; loadPos += blockDim.x)
	if(loadPos >= apronStart)
	{
		const int smemPos = loadPos - apronStart;
    gmem_ptr = get_elem(d_Data, row_idx, loadPos, float);
    shmem_ptr = data + smemPos;
    bool is_valid = (loadPos >= apronStartClamped) && (loadPos <= apronEndClamped);
    if (is_valid)
    {
#pragma unroll
      for (int idx = 0; idx < 3; ++idx)
      {
        *shmem_ptr = *gmem_ptr;
        gmem_ptr = make_step(gmem_ptr, in_gmem_plane_step, float);
        shmem_ptr = make_step(shmem_ptr, shmem_pitch, float);
      }
    }
    else
    {
#pragma unroll
      for (int idx = 0; idx < 3; ++idx)
      {
        *shmem_ptr = 0;
        shmem_ptr = make_step(shmem_ptr, shmem_pitch, float);
      }
    }
	}

	__syncthreads();
	const int writePos = tileStart + threadIdx.x;

	if(writePos <= tileEndClamped)
	{
		const int smemPos = writePos - apronStart;
    gmem_ptr = get_elem(d_Result, row_idx, writePos, float);
    shmem_ptr = data + smemPos;
#pragma unroll
    for (int idx = 0; idx < 3; ++idx)
    {
      *gmem_ptr = convolutionRow<2 * convKernelRadius>(shmem_ptr);
      gmem_ptr = make_step(gmem_ptr, out_gmem_plane_step, float);
      shmem_ptr = make_step(shmem_ptr, shmem_pitch, float);
    }
	}
}

__global__ void convolutionColumnGPU1to2(cudaPitchedPtr d_Result, cudaPitchedPtr d_Data,
                                         cudaPitchedPtr d_DataRow, int dataW,
                                         int dataH, int smemStride, int gmemStride)
{
	float rowValue;
  float *gmem_ptr, *out_gmem_ptr;

	const int columnStart = IMUL(blockIdx.x, convColumnTileWidth) + threadIdx.x;

	__shared__ float data[convColumnTileWidth * (convKernelRadius + convColumnTileHeight + convKernelRadius)];

	const int tileStart = IMUL(blockIdx.y, convColumnTileHeight);
	const int tileEnd = tileStart + convColumnTileHeight - 1;
	const int apronStart = tileStart - convKernelRadius;
	const int apronEnd = tileEnd   + convKernelRadius;

	const int tileEndClamped = min(tileEnd, dataH - 1);
	const int apronStartClamped = max(apronStart, 0);
	const int apronEndClamped = min(apronEnd, dataH - 1);

	int smemPos = IMUL(threadIdx.y, convColumnTileWidth) + threadIdx.x;

  int row_idx = apronStart + threadIdx.y;

	for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
		data[smemPos] = ((y >= apronStartClamped) && (y <= apronEndClamped)) ?
      *get_elem(d_Data, row_idx, columnStart, float) : 0.f;
		smemPos += smemStride;
    row_idx += blockDim.y;
	}

	__syncthreads();

  row_idx = tileStart + threadIdx.y;

	smemPos = IMUL(threadIdx.y + convKernelRadius, convColumnTileWidth) + threadIdx.x;
  gmem_ptr = get_elem(d_DataRow, row_idx, columnStart, float);

  size_t result_plane_step = d_Result.pitch * dataH;

	for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
		float sum = convolutionColumn<2 * convKernelRadius>(data + smemPos);
		rowValue = *gmem_ptr;

    out_gmem_ptr = get_elem(d_Result, row_idx, columnStart, float);
    *out_gmem_ptr = sqrtf(sum * sum + rowValue * rowValue);
    *make_step(out_gmem_ptr, result_plane_step, float) = atan2f(sum, rowValue) * RADTODEG;//TODO-> if semicerc
		smemPos += smemStride;
    gmem_ptr = make_step(gmem_ptr, blockDim.y * d_DataRow.pitch, float);
    row_idx += blockDim.y;
	}
}

__global__ void convolutionColumnGPU4to2(cudaPitchedPtr d_Result, cudaPitchedPtr d_Data,
                                         cudaPitchedPtr d_DataRow, int dataW, int dataH,
                                         int smemStride, int gmemStride)
{
  float *in_gmem_ptr, *out_gmem_ptr;

	const int columnStart = IMUL(blockIdx.x, convColumnTileWidth) + threadIdx.x;
  const bool is_valid = columnStart < dataW;

  const int num_elems = convColumnTileWidth * (convKernelRadius + convColumnTileHeight + convKernelRadius);
	__shared__ float data[3 * num_elems];

	const int tileStart = IMUL(blockIdx.y, convColumnTileHeight);
	const int tileEnd = tileStart + convColumnTileHeight - 1;
	const int apronStart = tileStart - convKernelRadius;
	const int apronEnd = tileEnd   + convKernelRadius;

	const int tileEndClamped = min(tileEnd, dataH - 1);
	const int apronStartClamped = max(apronStart, 0);
	const int apronEndClamped = min(apronEnd, dataH - 1);

	int smemPos = IMUL(threadIdx.y, convColumnTileWidth) + threadIdx.x;

  int row_idx = apronStart + threadIdx.y;
  float *img_ptr = get_elem(d_Data, row_idx, columnStart, float);
  size_t img_plane_step = d_Data.pitch * dataH;

	for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
    if ((y >= apronStartClamped) && (y <= apronEndClamped))
    {
#pragma unroll
      for (int plane_idx = 0; plane_idx < 3; ++plane_idx)
        data[smemPos + plane_idx * num_elems] = *make_step(img_ptr, plane_idx * img_plane_step, float);
    }
    else
    {
#pragma unroll
      for (int plane_idx = 0; plane_idx < 3; ++plane_idx)
        data[smemPos + plane_idx * num_elems] = 0;
    }
		smemPos += smemStride;
    img_ptr = make_step(img_ptr, blockDim.y * d_Data.pitch, float);
	}

	__syncthreads();

	smemPos = IMUL(threadIdx.y + convKernelRadius, convColumnTileWidth) + threadIdx.x;
  row_idx = tileStart + threadIdx.y;
  size_t convBuffer_plane_step = d_DataRow.pitch * dataH;
  size_t result_plane_step = d_Result.pitch * dataH;

  if (is_valid)
  for (int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
  {
    float sum_r, sum_g, sum_b;
    float row_value_r, row_value_g, row_value_b;
    sum_r = convolutionColumn<2 * convKernelRadius>(data + smemPos);
    sum_g = convolutionColumn<2 * convKernelRadius>(data + smemPos + num_elems);
    sum_b = convolutionColumn<2 * convKernelRadius>(data + smemPos + 2 * num_elems);
    in_gmem_ptr = get_elem(d_DataRow, row_idx, columnStart, float);
    row_value_r = *in_gmem_ptr;
    row_value_g = *(in_gmem_ptr = make_step(in_gmem_ptr, convBuffer_plane_step, float));
    row_value_b = *make_step(in_gmem_ptr, convBuffer_plane_step, float);

    float mag_r, mag_g, mag_b;
    mag_r = sqrtf(sum_r * sum_r + row_value_r * row_value_r);
    mag_g = sqrtf(sum_g * sum_g + row_value_g * row_value_g);
    mag_b = sqrtf(sum_b * sum_b + row_value_b * row_value_b);

    if ((mag_g >= mag_r) && (mag_g >= mag_b))
    {
      mag_r = mag_g;
      sum_r = sum_g;
      row_value_r = row_value_g;
    }
    else if ((mag_b >= mag_r) && (mag_b >= mag_g))
    {
      mag_r = mag_b;
      sum_r = sum_b;
      row_value_r = row_value_b;
    }

    float angle = atan2f(sum_r, row_value_r);
    angle = angle * 180 / PI + 180;

    out_gmem_ptr = get_elem(d_Result, row_idx, columnStart, float);
    *out_gmem_ptr = mag_r;
    *make_step(out_gmem_ptr, result_plane_step, float) = int(angle) % 180;//TODO-> if semicerc
    smemPos += smemStride;
    row_idx += blockDim.y;
  }
}

__host__ void HOGConvolution::initKernel()
{
  float *kernel = (float *)malloc(convKernelSize);
  kernel[0] = 1.0f; kernel[1] = 0;  kernel[2] = -1.0f;
  cutilSafeCall(cudaMemcpyToSymbol(d_Kernel, kernel, convKernelSize));
  free(kernel);
}

__host__ HOGConvolution::HOGConvolution(int in_width, int in_height,
                                        bool in_is_use_grayscale, cudaStream_t in_stream)
  : m_stream(in_stream), convUseGrayscale(in_is_use_grayscale)
{
  convWidth = in_width;
  convHeight = in_height;

  if (convUseGrayscale)
  {
    cutilSafeCall(cudaMallocPitch(&(convBuffer.ptr), &(convBuffer.pitch),
      sizeof(float)* in_width, in_height));
  }
  else
  {
    cutilSafeCall(cudaMallocPitch(&(convBuffer.ptr), &(convBuffer.pitch),
      sizeof(float)* in_width, 3 * in_height));
  }
  convBuffer.xsize = in_width;
  convBuffer.ysize = in_height;

  if (m_num_objects++ == 0)
    initKernel();
}

__host__ HOGConvolution::~HOGConvolution()
{
  --m_num_objects;
  cutilSafeCall(cudaFree(convBuffer.ptr));
}

__host__ void HOGConvolution::SetConvolutionSize(int width, int height)
{
  convWidth = width;
  convHeight = height;

  blockGridRows = dim3(iDivUp(convWidth, convRowTileWidth), convHeight);
  blockGridColumns = dim3(iDivUp(convWidth, convColumnTileWidth),
    iDivUp(convHeight, convColumnTileHeight));
  threadBlockRows = dim3(convRowTileWidth);
  threadBlockColumns = dim3(convColumnTileWidth, convColumnTileHeight);
}

__host__ void HOGConvolution::ComputeColorGradients1to2(cudaPitchedPtr inputImage, cudaPitchedPtr outputImage)
{
  convolutionRowGPU1 << <blockGridRows, threadBlockRows, 0, m_stream >> >(convBuffer, inputImage, convWidth, convHeight);
  convolutionColumnGPU1to2 << <blockGridColumns, threadBlockColumns, 0, m_stream >> >(
    outputImage, inputImage, convBuffer, convWidth, convHeight,
    convColumnTileWidth * threadBlockColumns.y, convWidth * threadBlockColumns.y);
}

__host__ void HOGConvolution::ComputeColorGradients4to2(cudaPitchedPtr inputImage, cudaPitchedPtr outputImage)
{
  convBuffer.xsize = convWidth;
  convBuffer.ysize = convHeight;
  convolutionRowGPU4 << <blockGridRows, threadBlockRows, 0, m_stream >> >(convBuffer, inputImage, convWidth, convHeight);
  convolutionColumnGPU4to2 << <blockGridColumns, threadBlockColumns, 0, m_stream >> >(
    outputImage, inputImage, convBuffer, convWidth, convHeight,
    convColumnTileWidth * threadBlockColumns.y, convWidth * threadBlockColumns.y);
}