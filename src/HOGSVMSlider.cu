#include "HOGSVMSlider.h"
#include "HOGUtils.h"

#include <cassert>
#include <fstream>
#include <iostream>

const int MAX_CONSTANT = 1024;
__device__ __constant__ float weights[MAX_CONSTANT];
texture<float, 1, cudaReadModeElementType> texSVM;
cudaArray *HOGSVMSlider::svmArray = 0;
cudaChannelFormatDesc HOGSVMSlider::channelDescSVM;
unsigned int HOGSVMSlider::m_num_objects = 0;
float HOGSVMSlider::svmBias;
bool HOGSVMSlider::m_is_set = false;

extern __shared__ float1 allSharedF1[];

__host__ void ReorderSVMWeights(float *svmWeights,
  int num_blocks_per_window_x, int num_blocks_per_window_y,
  int num_cells_per_block_x, int num_cells_per_block_y, int noHistogramBins)
{
  int num_elems = num_blocks_per_window_x * num_blocks_per_window_y *
    num_cells_per_block_x * num_cells_per_block_y * noHistogramBins;
  float *cache = new float[num_elems];
  memcpy(cache, svmWeights, num_elems * sizeof(float));
  for (int cell_y_idx = 0; cell_y_idx < num_cells_per_block_y; ++cell_y_idx)
  {
    for (int cell_x_idx = 0; cell_x_idx < num_cells_per_block_x; ++cell_x_idx)
    {
      for (int cell_bin_idx = 0; cell_bin_idx < noHistogramBins; ++cell_bin_idx)
      {
        for (int block_y_idx = 0; block_y_idx < num_blocks_per_window_y; ++block_y_idx)
        {
          for (int block_x_idx = 0; block_x_idx < num_blocks_per_window_x; ++block_x_idx)
          {
            int out_idx = (((cell_y_idx * num_cells_per_block_x + cell_x_idx) *
              noHistogramBins + cell_bin_idx) * num_blocks_per_window_y +
              block_y_idx) * num_blocks_per_window_x + block_x_idx;
            int in_idx = (((block_x_idx * num_blocks_per_window_y + block_y_idx) *
              num_cells_per_block_x + cell_x_idx) * num_cells_per_block_y +
              cell_y_idx) * noHistogramBins + cell_bin_idx;
            svmWeights[out_idx] = cache[in_idx];
          }
        }
      }
    }
  }
  delete[] cache;
}

HOGSVMSlider::HOGSVMSlider(cudaStream_t in_stream)
  : m_stream(in_stream)
{
}

HOGSVMSlider::~HOGSVMSlider()
{
}

inline __device__ void readData(cudaPitchedPtr &io_histograms,
                                cudaPitchedPtr out_cache,
                                int in_x, int in_y, int in_num_bins,
                                int in_step_x, int in_step_y)
{
  for (int bin_idx = 0; bin_idx < in_num_bins; ++bin_idx)
  {
    for (int row_idx = in_y; row_idx < out_cache.ysize; row_idx += in_step_y)
    {
      float *cache_ptr = get_row(out_cache, row_idx, float);
      if (row_idx < io_histograms.ysize)
      {
        float *gmem_ptr = get_row(io_histograms, row_idx, float);
        for (int idx = in_x; idx < out_cache.xsize; idx += in_step_x)
          if (idx < io_histograms.xsize)
            cache_ptr[idx] = gmem_ptr[idx];
          else
            cache_ptr[idx] = 0;
      }
      else
        for (int idx = 0; idx < out_cache.xsize; idx += in_step_x)
          cache_ptr[idx] = 0;
    }
    io_histograms.ptr = get_row(io_histograms, io_histograms.ysize, float);
    out_cache.ptr = get_row(out_cache, out_cache.ysize, float);
  }
}

__global__ void linearSVMEvaluation(cudaPitchedPtr svmScores, float svmBias,
                                    cudaPitchedPtr blockHistograms, int histogram_size,
                                    int numberOfBlockPerWindowX, int numberOfBlockPerWindowY,
                                    int num_bins_per_iteration)
{
	int texPos;
	float texValue;

  int num_windows_per_block_x = blockDim.x;
  int num_windows_per_block_y = blockDim.y;

  int local_window_x_idx = threadIdx.x;
  int local_window_y_idx = threadIdx.y;

  int window_x_base = blockIdx.x * num_windows_per_block_x;
  int window_y_base = blockIdx.y * num_windows_per_block_y;

  int window_x_idx = local_window_x_idx + window_x_base;
  int window_y_idx = local_window_y_idx + window_y_base;

	float res = 0;

  cudaPitchedPtr cache;
  cache.ptr = allSharedF1;
  cache.xsize = num_windows_per_block_x + numberOfBlockPerWindowX - 1;
  cache.ysize = num_windows_per_block_y + numberOfBlockPerWindowY - 1;
  cache.pitch = cache.xsize * sizeof(float);

  bool is_valid = (window_x_idx < svmScores.xsize) && (window_y_idx < svmScores.ysize);

  blockHistograms.ptr = get_elem(blockHistograms, window_y_base, window_x_base, float);
  blockHistograms.xsize -= window_x_base;
  blockHistograms.xsize = min((unsigned int)blockHistograms.xsize, (unsigned int)cache.xsize);
  blockHistograms.ysize -= window_y_base;
  blockHistograms.ysize = min((unsigned int)blockHistograms.ysize, (unsigned int)cache.ysize);

  for (int block_bin_idx = 0; block_bin_idx < histogram_size; block_bin_idx += num_bins_per_iteration)
  {
    // load data to the cache
    int num_bins_to_read = min(histogram_size - block_bin_idx, num_bins_per_iteration);
    readData(blockHistograms, cache, local_window_x_idx, local_window_y_idx,
      num_bins_to_read, num_windows_per_block_x, num_windows_per_block_y);

    __syncthreads();

    if (is_valid)
    {
      for (int bin_idx = 0; bin_idx < num_bins_per_iteration; ++bin_idx)
      {
        for (int block_y_idx = 0; block_y_idx < numberOfBlockPerWindowY; ++block_y_idx)
        {
          for (int block_x_idx = 0; block_x_idx < numberOfBlockPerWindowX; ++block_x_idx)
          {
            float val = *get_elem(cache, local_window_y_idx + block_y_idx +
              bin_idx * cache.ysize, block_x_idx + local_window_x_idx, float);

            texPos = ((block_bin_idx + bin_idx) * numberOfBlockPerWindowY + block_y_idx) *
              numberOfBlockPerWindowX + block_x_idx;
            texValue = tex1D(texSVM, texPos);
            res += val * texValue;
          }
        }
      }
    }
    __syncthreads();
  }

	if (is_valid)
	{
		res -= svmBias;
    *get_elem(svmScores, window_y_idx, window_x_idx, float) = res;
	}
}

__global__ void linearSVMEvaluationConstant(cudaPitchedPtr svmScores, float svmBias,
                                            cudaPitchedPtr blockHistograms, int histogram_size,
                                            int numberOfBlockPerWindowX, int numberOfBlockPerWindowY,
                                            int num_bins_per_iteration)
{
	int texPos;
	float texValue;

  int num_windows_per_block_x = blockDim.x;
  int num_windows_per_block_y = blockDim.y;

  int local_window_x_idx = threadIdx.x;
  int local_window_y_idx = threadIdx.y;

  int window_x_base = blockIdx.x * num_windows_per_block_x;
  int window_y_base = blockIdx.y * num_windows_per_block_y;

  int window_x_idx = local_window_x_idx + window_x_base;
  int window_y_idx = local_window_y_idx + window_y_base;

	float res = 0;

  cudaPitchedPtr cache;
  cache.ptr = allSharedF1;
  cache.xsize = num_windows_per_block_x + numberOfBlockPerWindowX - 1;
  cache.ysize = num_windows_per_block_y + numberOfBlockPerWindowY - 1;
  cache.pitch = cache.xsize * sizeof(float);

  bool is_valid = (window_x_idx < svmScores.xsize) && (window_y_idx < svmScores.ysize);

  blockHistograms.ptr = get_elem(blockHistograms, window_y_base, window_x_base, float);

  for (int block_bin_idx = 0; block_bin_idx < histogram_size; block_bin_idx += num_bins_per_iteration)
  {
    // load data to the cache
    readData(blockHistograms, cache, local_window_x_idx, local_window_y_idx,
      num_bins_per_iteration, num_windows_per_block_x, num_windows_per_block_y);

    __syncthreads();

    if (is_valid)
    {
      for (int bin_idx = 0; bin_idx < num_bins_per_iteration; ++bin_idx)
      {
        for (int block_y_idx = 0; block_y_idx < numberOfBlockPerWindowY; ++block_y_idx)
        {
          for (int block_x_idx = 0; block_x_idx < numberOfBlockPerWindowX; ++block_x_idx)
          {
            float val = *get_elem(cache, local_window_y_idx + block_y_idx +
              bin_idx * cache.ysize, block_x_idx + local_window_x_idx, float);

            texPos = ((block_bin_idx + bin_idx) * numberOfBlockPerWindowY + block_y_idx) *
              numberOfBlockPerWindowX + block_x_idx;
            texValue = weights[texPos];
            //texValue = tex1D(texSVM, texPos);
            res += val * texValue;
          }
        }
      }
    }
    __syncthreads();
  }

	if (is_valid)
	{
		res -= svmBias;
    *get_elem(svmScores, window_y_idx, window_x_idx, float) = res;
	}
}

__host__ void HOGSVMSlider::LinearSVMEvaluation(cudaPitchedPtr svmScores, cudaPitchedPtr blockHistograms,
                                                int in_histogram_size,
                                                int in_window_size_x, int in_window_size_y)
{
  if (!m_is_set)
    exit(-1);

  int num_bins_per_iteration = 3;
  int num_windows_per_block_x = 32;
  int num_windows_per_block_y = 16;
  num_windows_per_block_y = min(num_windows_per_block_y, (int)svmScores.ysize);

  dim3 threadCount = dim3(num_windows_per_block_x, num_windows_per_block_y, 1);
  dim3 blockCount = dim3(iDivUp(svmScores.xsize, num_windows_per_block_x),
    iDivUp(svmScores.ysize, num_windows_per_block_y));

  size_t shared_memory_size = num_bins_per_iteration *
    (num_windows_per_block_x + in_window_size_x - 1) *
    (num_windows_per_block_y + in_window_size_y - 1) * sizeof(float);

  if (in_window_size_x * in_window_size_y * in_histogram_size <= MAX_CONSTANT)
  {
    linearSVMEvaluationConstant<<<blockCount, threadCount, shared_memory_size, m_stream>>>
      (svmScores, svmBias, blockHistograms, in_histogram_size,
      in_window_size_x, in_window_size_y, num_bins_per_iteration);
  }
  else
  {
    linearSVMEvaluation<<<blockCount, threadCount, shared_memory_size, m_stream>>>
      (svmScores, svmBias, blockHistograms, in_histogram_size,
      in_window_size_x, in_window_size_y, num_bins_per_iteration);
  }
}

__host__ void HOGSVMSlider::SetWeights(float in_bias, float* in_weights, int in_num_elems,
                                       int in_num_blocks_x, int in_num_blocks_y,
                                       int in_block_size_x, int in_block_size_y, int in_num_bins)
{
  // release previous weights
  ReleaseWeights();

  // Set new weights
  channelDescSVM = cudaCreateChannelDesc<float>();
  float *tmp_weights = new float[in_num_elems];
  memcpy(tmp_weights, in_weights, in_num_elems * sizeof(float));
  ReorderSVMWeights(tmp_weights, in_num_blocks_x, in_num_blocks_y,
    in_block_size_x, in_block_size_y, in_num_bins);
	cutilSafeCall(cudaMallocArray(&svmArray, &channelDescSVM, in_num_elems, 1));
	cutilSafeCall(cudaMemcpyToArray(svmArray, 0, 0, tmp_weights,
    in_num_elems * sizeof(float), cudaMemcpyHostToDevice));
	svmBias = in_bias;
	cutilSafeCall(cudaBindTextureToArray(texSVM, svmArray, channelDescSVM));
  if (in_num_elems < MAX_CONSTANT)
  {
    cutilSafeCall(cudaMemcpyToSymbol(weights, tmp_weights,
      in_num_elems * sizeof(float), 0, cudaMemcpyHostToDevice));
  }
  delete [] tmp_weights;

  m_is_set = true;
}

__host__ void HOGSVMSlider::ReleaseWeights()
{
  if (m_is_set)
  {
    cutilSafeCall(cudaUnbindTexture(texSVM));
    cutilSafeCall(cudaFreeArray(svmArray));
    m_is_set = false;
  }
}

__host__ void HOGSVMSlider::UpdateSVMWeights(float in_bias, float* in_weights,
                               int in_num_elems, int in_num_blocks_x, int in_num_blocks_y,
                               int in_block_size_x, int in_block_size_y, int in_num_bins)
{
  SetWeights(in_bias, in_weights, in_num_elems, in_num_blocks_x, in_num_blocks_y,
    in_block_size_x, in_block_size_y, in_num_bins);
}

__host__ void HOGSVMSlider::CountWindows(int in_num_blocks_x, int in_num_blocks_y,
                                         int in_num_blocks_per_window_x, int in_num_blocks_per_window_y,
                                         int &out_num_windows_x, int &out_num_windows_y)
{
  out_num_windows_x = in_num_blocks_x - in_num_blocks_per_window_x + 1;
  out_num_windows_y = in_num_blocks_y - in_num_blocks_per_window_y + 1;
}
