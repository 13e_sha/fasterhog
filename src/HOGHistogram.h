#ifndef __HOG_HISTOGRAM__
#define __HOG_HISTOGRAM__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  include <windows.h>
#endif

#include <cuda_gl_interop.h>
#include "cutil_inline.h"
#include <cuda.h>

#include "HOGDefines.h"

class HOGHistogram
{
public:
  /// \brief
  /// The constructor
  ///
  /// \param[in] in_stream The stream to make computations
  __host__ HOGHistogram(cudaStream_t in_steam = 0);

  /// \brief
  /// The destructor
  __host__ ~HOGHistogram();

  __host__ void ComputeBlockHistogramsWithGauss(cudaPitchedPtr inputImage,
    cudaPitchedPtr blockHistograms,
    int noHistogramBins,
    int cellSizeX, int cellSizeY,
    int blockSizeX, int blockSizeY,
    int windowSizeX, int windowSizeY,
    int width, int height, int &out_num_blocks_x, int &out_num_blocks_y,
    int &out_num_windows_x, int &out_num_windows_y);
  __host__ void NormalizeBlockHistograms(cudaPitchedPtr blockHistograms,
    int noHistogramBins, int in_num_blocks_x, int in_num_blocks_y);

  /// \brief
  /// Returns number of blocks per image
  ///
  /// \param[in] in_width          The number of columns in the input feature map
  /// \param[in] in_height         The number of rows in the input feature map
  /// \param[in] in_cell_size_x    The number of pixels in the cell along X direction
  /// \param[in] in_cell_size_y    The number of pixels in the cell along Y direction
  /// \param[in] in_block_size_x   The number of cells in the block along X direction
  /// \param[in] in_block_size_y   The number of cells in the block along Y direction
  /// \param[out] out_num_blocks_x The number of blocks along X direction
  /// \param[out] out_num_blocks_y The number of blocks along Y direction
  __host__ static void CountNumBlocks(int in_width, int in_height,
    int in_cell_size_x, int in_cell_size_y, 
    int in_block_size_x, int in_block_size_y,
    int &out_num_blocks_x, int &out_num_blocks_y);
  
  /// \brief
  /// Initializes weights for histogram construction.
  ///
  /// \param[in] in_cellSizeX   The number of pixels in a cell along X direction
  /// \param[in] in_cellSizeY   The number of pixels in a cell along Y direction
  /// \param[in] in_blockSizeX  The number of cells in a block along X direction
  /// \param[in] in_blockSizeY  The number of cells in a block along Y direction
  /// \param[in] in_num_bins    The number of bins in the histogram
  /// \param[in] in_wtscale
  __host__ static void InitHistogram(int in_cellSizeX, int in_cellSizeY,
    int in_blockSizeX, int in_blockSizeY, int in_num_bins, float in_wtscale);

  /// \brief
  /// Deallocates used memory
  __host__ static void CloseHistogram();

private:

  static cudaArray* gaussArray;
  static cudaChannelFormatDesc channelDescGauss;
  /// Indicator of the state is set.
  static bool m_is_set;

  static unsigned int m_num_objects;

  cudaStream_t m_stream;
};

#endif
