#include "HOGPadding.h"
#include "HOGUtils.h"

HOGPadding::HOGPadding(cudaStream_t in_stream)
  : m_is_set(false), m_stream(in_stream)
{
}

HOGPadding::HOGPadding(int in_padded_width, int in_padded_height, cudaStream_t in_stream)
  : m_is_set(false), m_stream(in_stream)
{
  Init(in_padded_width, in_padded_height);
}

HOGPadding::~HOGPadding()
{
  Release();
}

void HOGPadding::Init(int in_padded_width, int in_padded_height, cudaStream_t in_stream)
{
  m_stream = in_stream;
  Release();
	cutilSafeCall(cudaMalloc((void**)&m_buffer,
    sizeof(uchar4) * in_padded_width * in_padded_height));
  m_is_set = true;
}

void HOGPadding::Release()
{
  if (m_is_set)
  {
    cutilSafeCall(cudaFree(m_buffer));
    m_is_set = false;
  }
}

__host__ void HOGPadding::PadHostImage(uchar4* registeredImage,
                                       float4 *paddedRegisteredImage,
                                       int minx, int miny, int maxx, int maxy,
                                       int in_hWidth, int in_hHeight,
                                       int in_avSizeX, int in_avSizeY,
                                       int in_marginX, int in_marginY,
                                       int &out_hPaddingSizeX, int &out_hPaddingSizeY,
                                       int &out_hPaddedWidth, int &out_hPaddedHeight,
                                       int &out_hWidthROI, int &out_hHeightROI)
{
	out_hWidthROI = maxx - minx;
	out_hHeightROI = maxy - miny;

	int toaddxx = 0, toaddxy = 0, toaddyx = 0, toaddyy = 0;

	if (in_avSizeX)
  {
    toaddxx = out_hWidthROI * in_marginX / in_avSizeX;
    toaddxy = out_hHeightROI * in_marginY / in_avSizeX;
  }
	if (in_avSizeY)
  {
    toaddyx = out_hWidthROI * in_marginX / in_avSizeY;
    toaddyy = out_hHeightROI * in_marginY / in_avSizeY; 
  }

	out_hPaddingSizeX = max(toaddxx, toaddyx); 
  out_hPaddingSizeY = max(toaddxy, toaddyy);

	out_hPaddedWidth = out_hWidthROI + out_hPaddingSizeX*2;
	out_hPaddedHeight = out_hHeightROI + out_hPaddingSizeY*2;

	cutilSafeCall(cudaMemsetAsync(m_buffer, 0,
    sizeof(uchar4) * out_hPaddedWidth * out_hPaddedHeight, m_stream));

  cutilSafeCall(cudaMemcpy2DAsync(m_buffer + out_hPaddingSizeX + out_hPaddingSizeY * out_hPaddedWidth,
			out_hPaddedWidth * sizeof(uchar4), registeredImage + minx + miny * in_hWidth,
			in_hWidth * sizeof(uchar4), out_hWidthROI * sizeof(uchar4),
			out_hHeightROI, cudaMemcpyHostToDevice, m_stream));

	Uchar4ToFloat4(m_buffer, paddedRegisteredImage, out_hPaddedWidth, out_hPaddedHeight, m_stream);
}
