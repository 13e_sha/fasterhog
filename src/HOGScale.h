#ifndef __HOG_SCALE__
#define __HOG_SCALE__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  include <windows.h>
#endif

#include <cuda_gl_interop.h>
#include <cutil_inline.h>
#include <cuda.h>

#include "HOGDefines.h"
#include "HOGUtils.h"

class HOGScale
{
public:
  __host__ static void InitScale(int in_width, int in_height, int in_idx);
  __host__ static void CloseScale(int in_idx);

  __host__ static void SetImage(float4 *in_image, int in_width, int in_height,
    int in_idx, cudaStream_t in_stream = 0);
  __host__ static void ReleaseImage();

  /// \brief
  /// Returns size of the resized image
  ///
  /// Returns size of the resized image.
  /// \param[in]  in_width   The number of columns in the resized image
  /// \param[in]  in_height  The number of rows in the original image
  /// \param[in]  in_scale   The scale to resize the image
  /// \param[out] out_width  The number of columns in the resized image
  /// \param[out] out_height The number of rows in the resized image
  __host__ static void GetResizedSize(int in_width, int in_height,
    float in_scale, int &out_width, int &out_height);

  __host__ HOGScale(int in_width, int in_height, cudaStream_t in_stream = 0);

  __host__ ~HOGScale();

  __host__ void DownscaleImage(float scale,
    bool useGrayscale, float4* paddedRegisteredImage,
    cudaPitchedPtr resizedPaddedImage, int &out_width, int &out_height);

private:
  int m_width;
  int m_height;

  /// cuda stream object to process.
  cudaStream_t m_stream;

  /// indicates that the memory was allocated
  static bool m_is_alocated[2];

  static bool m_is_bound;

  /// array to store original image
  static cudaArray *imageArray[2];

  static cudaChannelFormatDesc channelDescDownscale;
};

#endif
