#include <stdio.h>
#include <cstdlib>

#include "ResponsesAdapter.h"
#include "HOGDefines.h"

inline void safeCall(bool in_condition, const char *in_message)
{
  if (!in_condition) {
    fprintf(stderr, "%s(%i) : Runtime API error: %s.\n", __FILE__, __LINE__,
      in_message);
    exit(-1);
  }
}

ResponsesAdapter::ResponsesAdapter()
  : m_memory(nullptr), m_size(0)
{
}

ResponsesAdapter::~ResponsesAdapter()
{
}

cudaPitchedPtr ResponsesAdapter::GetLevel(int in_level)
{
  auto num_levels = (int)m_width_array.size();
  safeCall((in_level >= 0) && (in_level < num_levels), "Wrong index of the level");
  safeCall(isSet(), "Adapter parameters were not specified");

  cudaPitchedPtr res;
  res.ptr = make_step(m_memory, m_start_array[in_level], float);
  res.pitch = m_pitch_array[in_level];
  res.xsize = m_width_array[in_level];
  res.ysize = m_height_array[in_level];
  return res;
}

size_t ResponsesAdapter::GetSize() const
{
  return m_size;
}

float *ResponsesAdapter::GetAllResponses()
{
  return m_memory;
}

void ResponsesAdapter::SetMemory(float *in_memory)
{
  m_memory = in_memory;
}

void ResponsesAdapter::SetPyramidParameters(vector<int> in_width_array,
                                            vector<int> in_height_array)
{ 
  safeCall(in_width_array.size() == in_width_array.size(),
    "Sizes of input vectors should be equal");

  auto num_levels = (int)in_width_array.size();
  
  m_start_array.resize(num_levels);
  m_width_array.resize(num_levels);
  m_height_array.resize(num_levels);
  m_pitch_array.resize(num_levels);

  const size_t CACHE_LINE_WIDTH = 512;

  size_t start = 0;
  for (auto level_idx = 0; level_idx < num_levels; ++level_idx)
  {
    m_start_array[level_idx] = start;
    m_width_array[level_idx] = in_width_array[level_idx];
    m_height_array[level_idx] = in_height_array[level_idx];
    size_t line_width = in_width_array[level_idx] * sizeof(float);
    m_pitch_array[level_idx] =
      ((line_width - 1) / CACHE_LINE_WIDTH + 1) * CACHE_LINE_WIDTH;
    start += m_height_array[level_idx] * m_pitch_array[level_idx];
  }
  m_size = start;
}

bool ResponsesAdapter::isSet() const
{
  return (m_memory != nullptr) && (m_width_array.size() > 0) &&
    (m_height_array.size() > 0) && (m_start_array.size() > 0) &&
    (m_pitch_array.size() > 0);
}
