#ifndef __CUDA_HOG__
#define __CUDA_HOG__

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  include <windows.h>
#endif

#include <cuda_gl_interop.h>
#include <cutil_inline.h>
#include <cuda.h>

#include "HOGDefines.h"
#include "HOGPadding.h"
#include "HOGUtils.h"
#include "HOGConvolution.h"
#include "HOGHistogram.h"
#include "HOGSVMSlider.h"
#include "HOGScale.h"
#include "ResponsesAdapter.h"

#include <vector>

using std::vector;

struct ProcessingParams
{
  int padded_width;
  int padded_height;
  float min_scale;
  float max_scale;
  int num_scales;
};

class HOGEngineDevice
{
public:
  /// \brief
  /// The default constructor.
  __host__ HOGEngineDevice();

  /// \brief
  /// The destructor
  __host__ ~HOGEngineDevice();

  /// \brief
  /// Reset weights of the SVM
  ///
  /// \param[in] in_bias         The bias term.
  /// \param[in] in_weights      The array of weights.
  /// \param[in] in_num_elems    The number of weights.
  /// \param[in] in_num_blocks_x The number of blocks per window along X direction.
  /// \param[in] in_num_blocks_y The number of blocks per window along Y direction.
  /// \param[in] in_block_size_x The number of cells per block along X direction.
  /// \param[in] in_block_size_y The number of cells per block along Y direction.
  /// \param[in] in_num_bins     The number of bins in the histogram
  __host__ void SetSVMWeights(float in_bias, float* in_weights,
    int in_num_elems, int in_num_blocks_x, int in_num_blocks_y,
    int in_block_size_x, int in_block_size_y, int in_num_bins);

  __host__ void InitHOG(int width, int height, int avSizeX, int avSizeY,
    int marginX, int marginY, int cellSizeX, int cellSizeY,
    int blockSizeX, int blockSizeY, int windowSizeX, int windowSizeY,
    int noOfHistogramBins, float wtscale, float svmBias, float* svmWeights,
    int svmWeightsCount, bool useGrayscale);

  __host__ void CloseHOG();

  __host__ void BeginHOGProcessing(unsigned char* hostImage,
    int minx, int miny, int maxx, int maxy, float minScale,
    float maxScale);

  __host__ ResponsesAdapter &EndHOGProcessing();

  __host__ ResponsesAdapter &ContinueHOGProcessing(unsigned char* hostImage, int minx,
    int miny, int maxx, int maxy, float minScale, float maxScale);

  __host__ void GetHOGParameters(float *cStartScale, float *cEndScale,
    float *cScaleRatio, int *cScaleCount,
    int *cPaddingSizeX, int *cPaddingSizeY,
    int *cPaddedWidth, int *cPaddedHeight, int *cNoOfCellsX, int *cNoOfCellsY,
    int *cNoOfBlocksX, int *cNoOfBlocksY,
    int *cNumberOfWindowsX, int *cNumberOfWindowsY,
    int *cNumberOfBlockPerWindowX, int *cNumberOfBlockPerWindowY);
private:
  /// Indicates the image is being processed
  bool m_is_working;

  /// the object to pad image
  HOGPadding m_padding;

  /// the padded version of the input image.
  vector<float4 *>m_image_array;
  /// the results of the processing on the host
  vector<float*> m_host_scores_array;
  /// the results of the processing on the device
  vector<cudaPitchedPtr> m_device_scores_array;

  /// the indicator of the set to be set.
  bool m_is_set;

  /// the number of cuda streams to perform computations.
  int m_num_streams; 

  /// index of used buffers
  int m_idx;

  vector<HOGScale*> scale_objects;
  vector<HOGConvolution*> conv_objects;
  vector<HOGHistogram*> histogram_objects;
  vector<HOGSVMSlider*> slider_objects;
  vector<cudaPitchedPtr> resized_images;
  vector<cudaPitchedPtr> gradient_array;
  vector<cudaPitchedPtr> histogram_array;
  vector<cudaStream_t> m_stream_array;
  vector<ResponsesAdapter> m_adapter;

  /// stream for io operations
  cudaStream_t m_io_stream;

  int hWidth, hHeight;
  int hWidthROI, hHeightROI;
  int hPaddedWidth, hPaddedHeight;

  int hNoHistogramBins, rNoHistogramBins;

  int hPaddingSizeX, hPaddingSizeY;
  int hCellSizeX, hCellSizeY, hBlockSizeX, hBlockSizeY, hWindowSizeX, hWindowSizeY;
  int hNoOfCellsX, hNoOfCellsY, hNoOfBlocksX, hNoOfBlocksY;

  int hNumberOfBlockPerWindowX, hNumberOfBlockPerWindowY;
  int hNumberOfWindowsX, hNumberOfWindowsY;

  bool hUseGrayscale;

  float scaleRatio;
  /// the minimal scale
  float m_min_scale;
  /// the maximal scale
  float m_max_scale;
  /// the maximal number of scales.
  int m_num_scales;

  int avSizeX, avSizeY, marginX, marginY;

  /// \brief
  /// Chooses number of streams for the specified device
  ///
  /// \param[in] in_device_id is an identifier of the device
  /// \return number of streams
  unsigned int chooseNumStreams(int in_device_id) const;

  /// \brief
  /// Initializes streams 
  ///
  /// Initializes objects to process image in several streams
  void initStreams(int in_num_streams);

  /// \brief
  /// Releases streams
  ///
  /// Releases objects used for image processing in several streams.
  void releaseStreams();

  /// \brief
  /// Computes number of scales
  ///
  /// \param[in] in_min_scale   The minimal scale.
  /// \param[in] in_max_scale   The maximal scale.
  /// \param[in] in_ratio       The ratio of two consecutive scales.
  /// \return                   The number of scales.
  unsigned int getNumScales(float in_min_scale, float in_max_scale,
    float in_ratio) const;

  /// \brief
  /// Sets data to process
  ///
  /// \param[in]  in_host_image The pointer to host image to process.
  /// \param[in]  in_minx       The location of the leftmost pixel to process.
  /// \param[in]  in_miny       The location of the topmost pixel to process.
  /// \param[in]  in_maxx       The location of the rightmost pixel to process.
  /// \param[in]  in_maxy       The location of the bottommost pixel to process.
  /// \param[in]  in_min_scale  The minimal scale to process.
  /// \param[in]  in_max_scale  The maximal scale to process.
  /// \param[in]  in_idx        The index of parameters to set.
  /// \param[out] out_params    The parameters of the processing.
  void initProcessing(unsigned char* in_host_image, int in_minx, int in_miny,
    int in_maxx, int in_maxy, float in_min_scale, float in_max_scale,
    int in_idx, ProcessingParams &out_params);

  /// \brief
  /// Process data that was set.
  ///
  /// \param[in]     in_idx    The index of parameters to process.
  /// \param[in,out] io_params The parameters of the processing.
  void process(int in_idx, ProcessingParams &io_params);

  /// \brief
  /// Returns results of the processing
  ///
  /// \param[in]  in_idx    The index of parameters to process.
  /// \return               The array of the SVM scores.
  ResponsesAdapter &finalizeProcessing(int in_idx);

  /// \brief
  /// Initializes adapter to access to responses maps
  ///
  /// \param[in] in_idx    The index of the adapter to initialize.
  /// \param[in] in_params The parameters of the processing.
  void initResponesesAdapter(int in_idx, ProcessingParams in_params);
};

#endif
