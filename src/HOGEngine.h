#ifndef __HOG_ENGINE__
#define __HOG_ENGINE__

#include "HOGNMS.h"
#include "IHOGEngine.h"
#include "HOGEngineDevice.h"

#include <string>

using namespace std;

namespace HOG
{
#ifdef _WIN32
  class __declspec(dllexport) HOGEngine : public IHOGEngine
#else
  class HOGEngine : public IHOGEngine
#endif
	{
	private:
		static HOGEngine* instance;

		int iDivUpF(int a, float b);

		HOGNMS* nmsProcessor;
		void readSVMFromFile(std::string fileName);

		HOGResult map2Bbox(int in_row, int in_col, float in_scale);

    HOGEngineDevice m_engine;

	public:
		int imageWidth, imageHeight;

		int avSizeX, avSizeY, marginX, marginY;

		int scaleCount;
		int hCellSizeX, hCellSizeY;
		int hBlockSizeX, hBlockSizeY;
		int hWindowSizeX, hWindowSizeY;
		int hNoOfHistogramBins;
		int hPaddedWidth, hPaddedHeight;
		int hPaddingSizeX, hPaddingSizeY;

		int minX, minY, maxX, maxY;

		float wtScale;

		float startScale, endScale, scaleRatio;

		int svmWeightsCount;
		float svmBias, *svmWeights;

		int hNoOfCellsX, hNoOfCellsY;
		int hNoOfBlocksX, hNoOfBlocksY;
		int hNumberOfWindowsX, hNumberOfWindowsY;
		int hNumberOfBlockPerWindowX, hNumberOfBlockPerWindowY;

		bool useGrayscale;

		float* cppResult;

		enum ImageType
		{
			IMAGE_RESIZED,
			IMAGE_COLOR_GRADIENTS,
			IMAGE_GRADIENT_ORIENTATIONS,
			IMAGE_PADDED,
			IMAGE_ROI
		};

		static HOGEngine* Instance(void) {
			if (instance == NULL) instance = new HOGEngine();
			return instance;
		}

		void InitializeHOG(int iw, int ih, float svmBias, float* svmWeights, int svmWeightsCount);
		void InitializeHOG(int iw, int ih, std::string fileName);

		void FinalizeHOG();

		void BeginProcess(HOGImage* hostImage, int _minx = -1, int _miny = -1, int _maxx = -1, int _maxy = -1,
			float minScale = -1.0f, float maxScale = -1.0f);
		void EndProcess();

    /// \brief
    /// Finalizes processing of the previous image and starts processing of the
    /// current image in an asynchronous way.
    /// 
    /// \param[in]  in_image            The next image to process.
    /// \param[in]  in_min_x            The location of a leftmost pixel to process.
    /// \param[in]  in_min_y            The location of a topmost pixel to process.
    /// \param[in]  in_max_x            The location of a rightmost pixel to process.
    /// \param[in]  in_max_y            The location of a bottommost pixel to process.
    /// \param[in]  in_min_scale        The minimal scale to process.
    /// \param[in]  in_max_scale        The maximal scale to process.
    /// \return                         The status of the operation.
    void ContinueProcess(HOGImage* in_image,
      int in_min_x = -1, int in_min_y = -1,
      int in_max_x = -1, int in_max_y = -1,
      float in_min_scale = -1.0f, float in_max_scale = -1.0f);

		void ComputeFormattedResults(ResponsesAdapter &in_adapter);

		void SaveResultsToDisk(char* fileName);

		HOGEngine(void) { }
		~HOGEngine(void) { }
	};
}

#endif
