#include "HOGEngineDevice.h"
#include "HOGUtils.h"
#include "HOGConvolution.h"
#include "HOGHistogram.h"
#include "HOGSVMSlider.h"
#include "HOGScale.h"
#include "HOGPadding.h"


__host__ HOGEngineDevice::HOGEngineDevice()
  : m_is_set(false), m_is_working(false), m_idx(0)
{
  m_image_array.resize(2);
  m_host_scores_array.resize(2);
  m_device_scores_array.resize(2);
  m_adapter.resize(2);
}

__host__ HOGEngineDevice::~HOGEngineDevice()
{
  CloseHOG();
}


__host__ void HOGEngineDevice::SetSVMWeights(float in_bias, float* in_weights,
                                             int in_num_elems, int in_num_blocks_x,
                                             int in_num_blocks_y, int in_block_size_x,
                                             int in_block_size_y, int in_num_bins)
{
  HOGSVMSlider::UpdateSVMWeights(in_bias, in_weights, in_num_elems,
    in_num_blocks_x, in_num_blocks_y, in_block_size_x, in_block_size_y,
    in_num_bins);
}

__host__ void HOGEngineDevice::InitHOG(int width, int height,
                                       int _avSizeX, int _avSizeY,
                                       int _marginX, int _marginY,
                                       int cellSizeX, int cellSizeY,
                                       int blockSizeX, int blockSizeY,
                                       int windowSizeX, int windowSizeY,
                                       int noOfHistogramBins, float wtscale,
                                       float svmBias, float* svmWeights, int svmWeightsCount,
                                       bool useGrayscale)
{
  int device_id = cutGetMaxGflopsDeviceId();
	cudaSetDevice(device_id);

  CloseHOG();

	int toaddxx = 0, toaddxy = 0, toaddyx = 0, toaddyy = 0;

	hWidth = width; hHeight = height;
	avSizeX = _avSizeX; avSizeY = _avSizeY; marginX = _marginX; marginY = _marginY;

	if (avSizeX) 
  {
    toaddxx = hWidth * marginX / avSizeX;
    toaddxy = hHeight * marginY / avSizeX;
  }
	if (avSizeY)
  {
    toaddyx = hWidth * marginX / avSizeY;
    toaddyy = hHeight * marginY / avSizeY;
  }

	hPaddingSizeX = max(toaddxx, toaddyx);
  hPaddingSizeY = max(toaddxy, toaddyy);

	hPaddedWidth = hWidth + hPaddingSizeX*2;
	hPaddedHeight = hHeight + hPaddingSizeY*2;

	hUseGrayscale = useGrayscale;

	hNoHistogramBins = noOfHistogramBins;
	hCellSizeX = cellSizeX; hCellSizeY = cellSizeY; hBlockSizeX = blockSizeX; hBlockSizeY = blockSizeY;
	hWindowSizeX = windowSizeX; hWindowSizeY = windowSizeY;

	hNoOfCellsX = hPaddedWidth / cellSizeX;
	hNoOfCellsY = hPaddedHeight / cellSizeY;

	hNoOfBlocksX = hNoOfCellsX - blockSizeX + 1;
	hNoOfBlocksY = hNoOfCellsY - blockSizeY + 1;

	hNumberOfBlockPerWindowX = (windowSizeX - cellSizeX * blockSizeX) / cellSizeX + 1;
	hNumberOfBlockPerWindowY = (windowSizeY - cellSizeY * blockSizeY) / cellSizeY + 1;

  hNumberOfWindowsX = hNoOfBlocksX - hNumberOfBlockPerWindowX + 1;
  hNumberOfWindowsY = hNoOfBlocksY - hNumberOfBlockPerWindowY + 1;

	scaleRatio = 1.05f;
	m_min_scale = 1.0f;
	m_max_scale = min(hPaddedWidth / (float)hWindowSizeX,
    hPaddedHeight / (float)hWindowSizeY);
  m_num_scales = getNumScales(m_min_scale, m_max_scale, scaleRatio);


  SetSVMWeights(svmBias, svmWeights, svmWeightsCount,
    hNumberOfBlockPerWindowX, hNumberOfBlockPerWindowY,
    hBlockSizeX, hBlockSizeY, hNoHistogramBins);
  HOGHistogram::InitHistogram(hCellSizeX, hCellSizeY, hBlockSizeX, hBlockSizeY,
    hNoHistogramBins, wtscale);
  for (int idx = 0; idx < 2; ++idx)
  {
    HOGScale::InitScale(hPaddedWidth, hPaddedHeight, idx);
  	cutilSafeCall(cudaMalloc((void**)&(m_image_array[idx]), sizeof(float4) * hPaddedWidth * hPaddedHeight));
    // allocate memory for the scores
    m_device_scores_array[idx].xsize = hNumberOfWindowsX;
    m_device_scores_array[idx].ysize = hNumberOfWindowsY;
    cutilSafeCall(cudaMallocPitch(&m_device_scores_array[idx].ptr,
      &m_device_scores_array[idx].pitch, sizeof(float) * hNumberOfWindowsX,
      hNumberOfWindowsY * m_num_scales));
    cutilSafeCall(cudaMallocHost((void**)&(m_host_scores_array[idx]),
      m_device_scores_array[idx].pitch * m_num_scales * hNumberOfWindowsY));
  }
  cutilSafeCall(cudaStreamCreate(&m_io_stream));
  m_padding.Init(hPaddedWidth, hPaddedHeight, m_io_stream);

  int num_streams = chooseNumStreams(device_id);
  initStreams(num_streams);
  m_is_set = true;
}

__host__ void HOGEngineDevice::CloseHOG()
{
  if (m_is_set)
  {
    cudaDeviceSynchronize();


    for (int idx = 0; idx < 2; ++idx)
    {
      HOGScale::CloseScale(idx);
      cutilSafeCall(cudaFree(m_image_array[idx]));
      cutilSafeCall(cudaFreeHost(m_host_scores_array[idx]));
      cutilSafeCall(cudaFree(m_device_scores_array[idx].ptr));
    }
    HOGSVMSlider::ReleaseWeights();
    HOGHistogram::CloseHistogram();
    m_padding.Release();

    releaseStreams();
    cutilSafeCall(cudaStreamDestroy(m_io_stream));

    cudaDeviceReset();
    m_is_set = true;
  }
}

__host__ void HOGEngineDevice::BeginHOGProcessing(unsigned char* hostImage,
                                                  int minx, int miny,
                                                  int maxx, int maxy,
                                                  float minScale, float maxScale)
{
  if (!m_is_set)
    exit(-1);

  if (m_is_working)
    exit(-1);

  m_idx = (m_idx + 1) % 2;

  ProcessingParams params;
  initProcessing(hostImage, minx, miny, maxx, maxy, minScale, maxScale, m_idx, params);
  initResponesesAdapter(m_idx, params);
  cutilSafeCall(cudaStreamSynchronize(m_io_stream));

  m_is_working = true;

  process(m_idx, params);
}

__host__ ResponsesAdapter &HOGEngineDevice::EndHOGProcessing()
{
  ResponsesAdapter &result = finalizeProcessing(m_idx);
  cudaStreamSynchronize(m_io_stream);
  m_is_working = false;
  return result;
}

__host__ ResponsesAdapter &HOGEngineDevice::ContinueHOGProcessing(
  unsigned char* hostImage, int minx, int miny, int maxx, int maxy,
  float minScale, float maxScale)
{
  cudaStreamSynchronize(m_io_stream);
  if (!m_is_working)
    exit(-1);

  int next_idx = (m_idx + 1) % 2;
  ProcessingParams params;
  initProcessing(hostImage, minx, miny, maxx, maxy, minScale, maxScale, next_idx, params);

  ResponsesAdapter &result = finalizeProcessing(m_idx);
  if (!m_is_set)
    exit(-1);

  initResponesesAdapter(next_idx, params);

  process(next_idx, params);

  m_idx = next_idx;

  cudaStreamSynchronize(m_io_stream);
  return result;
}


__host__ void HOGEngineDevice::GetHOGParameters(float *cStartScale, float *cEndScale, float *cScaleRatio, int *cScaleCount,
							   int *cPaddingSizeX, int *cPaddingSizeY, int *cPaddedWidth, int *cPaddedHeight,
							   int *cNoOfCellsX, int *cNoOfCellsY, int *cNoOfBlocksX, int *cNoOfBlocksY,
							   int *cNumberOfWindowsX, int *cNumberOfWindowsY,
							   int *cNumberOfBlockPerWindowX, int *cNumberOfBlockPerWindowY)
{
	*cStartScale = m_min_scale;
	*cEndScale = m_max_scale;
	*cScaleRatio = scaleRatio;
  *cScaleCount = m_num_scales;
	*cPaddingSizeX = hPaddingSizeX;
	*cPaddingSizeY = hPaddingSizeY;
	*cPaddedWidth = hPaddedWidth;
	*cPaddedHeight = hPaddedHeight;
	*cNoOfCellsX = hNoOfCellsX;
	*cNoOfCellsY = hNoOfCellsY;
	*cNoOfBlocksX = hNoOfBlocksX;
	*cNoOfBlocksY = hNoOfBlocksY;
	*cNumberOfWindowsX = hNumberOfWindowsX;
	*cNumberOfWindowsY = hNumberOfWindowsY;
	*cNumberOfBlockPerWindowX = hNumberOfBlockPerWindowX;
	*cNumberOfBlockPerWindowY = hNumberOfBlockPerWindowY;
}

__host__ unsigned int HOGEngineDevice::chooseNumStreams(int in_device_id) const
{
  cudaDeviceProp info;
  cudaGetDeviceProperties(&info, in_device_id);
  unsigned int result;
  if (info.totalGlobalMem < 1.5 * (1 << 30)) // 1.5Gb
    result = 2;
  else
    result = 4;
  return result;
}

__host__ void HOGEngineDevice::initStreams(int in_num_streams)
{
  m_num_streams = in_num_streams;
  m_stream_array.resize(in_num_streams);
  scale_objects.resize(in_num_streams);
  conv_objects.resize(in_num_streams);
  histogram_objects.resize(in_num_streams);
  slider_objects.resize(in_num_streams);
  resized_images.resize(in_num_streams);
  gradient_array.resize(in_num_streams);
  histogram_array.resize(in_num_streams);
  for (int stream_idx = 0; stream_idx < in_num_streams; ++stream_idx)
  {
    cudaStreamCreate(&(m_stream_array[stream_idx]));
    scale_objects[stream_idx] = new HOGScale(hPaddedWidth, hPaddedHeight,
      m_stream_array[stream_idx]);
    conv_objects[stream_idx] = new HOGConvolution(hPaddedWidth, hPaddedHeight,
      hUseGrayscale, m_stream_array[stream_idx]);
    histogram_objects[stream_idx] = new HOGHistogram(m_stream_array[stream_idx]);
    slider_objects[stream_idx] = new HOGSVMSlider(m_stream_array[stream_idx]);
    cudaPitchedPtr &img = resized_images[stream_idx];
    if (hUseGrayscale)
    {
      cutilSafeCall(cudaMallocPitch(&img.ptr, &img.pitch,
        sizeof(float) * hPaddedWidth, hPaddedHeight));
    }
    else
    {
      cutilSafeCall(cudaMallocPitch(&img.ptr, &img.pitch,
        sizeof(float) * hPaddedWidth, 3 * hPaddedHeight));
    }

    cudaPitchedPtr &gradient = gradient_array[stream_idx];
    cutilSafeCall(cudaMallocPitch(&gradient.ptr, &gradient.pitch,
      hPaddedWidth * sizeof(float), 2 * hPaddedHeight));
    gradient.xsize = hPaddedWidth;
    gradient.ysize = hPaddedHeight;

    cudaPitchedPtr &histogram = histogram_array[stream_idx];
    cutilSafeCall(cudaMallocPitch(&histogram.ptr, &histogram.pitch,
      hNoOfBlocksX * sizeof(float), hNoOfBlocksY * hCellSizeX * hCellSizeY * hNoHistogramBins));
    histogram.xsize = hNoOfBlocksX;
    histogram.ysize = hNoOfBlocksY;
  }
}

__host__ void HOGEngineDevice::releaseStreams()
{
  cudaDeviceSynchronize();
  int num_streams = m_stream_array.size();
  for (int stream_idx = 0; stream_idx < num_streams; ++stream_idx)
  {
    delete scale_objects[stream_idx];
    delete conv_objects[stream_idx];
    delete histogram_objects[stream_idx];
    delete slider_objects[stream_idx];

    cudaStreamDestroy(m_stream_array[stream_idx]);
    cutilSafeCall(cudaFree(resized_images[stream_idx].ptr));
    cutilSafeCall(cudaFree(gradient_array[stream_idx].ptr));
    cutilSafeCall(cudaFree(histogram_array[stream_idx].ptr));
  }
}

unsigned int HOGEngineDevice::getNumScales(float in_min_scale, float in_max_scale,
                                           float in_ratio) const
{
  return (int)floor(logf(in_max_scale/in_min_scale)/logf(in_ratio)) + 1;
}

void HOGEngineDevice::initProcessing(unsigned char* in_host_image,
  int in_minx, int in_miny, int in_maxx, int in_maxy, float in_min_scale,
  float in_max_scale, int in_idx, ProcessingParams &out_params)
{
  m_padding.PadHostImage((uchar4*)in_host_image, m_image_array[in_idx],
    in_minx, in_miny, in_maxx, in_maxy, hWidth, hHeight, avSizeX, avSizeY,
    marginX, marginY, hPaddingSizeX, hPaddingSizeY,
    hPaddedWidth, hPaddedHeight, hWidthROI, hHeightROI);

  out_params.padded_width = hPaddedWidth;
  out_params.padded_height = hPaddedHeight;
  out_params.min_scale = (in_min_scale < 0.0f) ? m_min_scale : in_min_scale;
  out_params.max_scale = (in_max_scale < 0.0f) ? m_max_scale : in_max_scale;
  out_params.num_scales = getNumScales(out_params.min_scale, out_params.max_scale, scaleRatio);

  HOGScale::SetImage(m_image_array[in_idx], hPaddedWidth, hPaddedHeight, in_idx, m_io_stream);
}

void HOGEngineDevice::process(int in_idx, ProcessingParams &io_params)
{
  int rNoOfBlocksX, rNoOfBlocksY;
  int num_windows_x, num_windows_y;

  float currentScale = io_params.min_scale;

  // pointer to the scores on the current level
  cudaPitchedPtr scores;

  int histogram_size = hNoHistogramBins * hBlockSizeX * hBlockSizeY;

  m_adapter[in_idx].SetMemory((float*)m_device_scores_array[in_idx].ptr);

  for (int i = 0; i < io_params.num_scales; i += m_num_streams)
  {
    for (int stream_idx = 0; stream_idx < m_num_streams; ++stream_idx)
    {
      int scale_idx = i + stream_idx;
      if (scale_idx >= io_params.num_scales)
        break;

      scale_objects[stream_idx]->DownscaleImage(currentScale, hUseGrayscale,
        m_image_array[in_idx], resized_images[stream_idx],
        io_params.padded_width, io_params.padded_height);
      conv_objects[stream_idx]->SetConvolutionSize(io_params.padded_width,
        io_params.padded_height);
      if (hUseGrayscale)
      {
        conv_objects[stream_idx]->ComputeColorGradients1to2(
          resized_images[stream_idx], gradient_array[stream_idx]);
      }
      else
      {
        conv_objects[stream_idx]->ComputeColorGradients4to2(
          resized_images[stream_idx], gradient_array[stream_idx]);
      }
      histogram_objects[stream_idx]->ComputeBlockHistogramsWithGauss(
        gradient_array[stream_idx], histogram_array[stream_idx],
        hNoHistogramBins, hCellSizeX, hCellSizeY, hBlockSizeX, hBlockSizeY,
        hWindowSizeX, hWindowSizeY, io_params.padded_width, io_params.padded_height,
        rNoOfBlocksX, rNoOfBlocksY, num_windows_x, num_windows_y);
      histogram_objects[stream_idx]->NormalizeBlockHistograms(
        histogram_array[stream_idx], histogram_size, rNoOfBlocksX, rNoOfBlocksY);

      scores = m_adapter[in_idx].GetLevel(scale_idx);
      histogram_array[stream_idx].xsize = rNoOfBlocksX;
      histogram_array[stream_idx].ysize = rNoOfBlocksY;
      slider_objects[stream_idx]->LinearSVMEvaluation(scores,
        histogram_array[stream_idx], histogram_size,
        hNumberOfBlockPerWindowX, hNumberOfBlockPerWindowY);

      // go to the next scale
      currentScale *= scaleRatio;
    }
  }
}

ResponsesAdapter &HOGEngineDevice::finalizeProcessing(int in_idx)
{
  cudaDeviceSynchronize();
  m_adapter[in_idx].SetMemory(m_host_scores_array[in_idx]);
  cutilSafeCall(cudaMemcpyAsync(m_host_scores_array[in_idx],
    m_device_scores_array[in_idx].ptr, m_adapter[in_idx].GetSize(),
    cudaMemcpyDeviceToHost, m_io_stream));
  return m_adapter[in_idx];
}

void HOGEngineDevice::initResponesesAdapter(int in_idx,
                                            ProcessingParams in_params)
{
  float currentScale = in_params.min_scale;

  vector<int> map_width_array(in_params.num_scales);
  vector<int> map_height_array(in_params.num_scales);

  for (int i = 0; i < in_params.num_scales; ++i)
  {
    int width, height;
    int num_blocks_x, num_blocks_y;
    HOGScale::GetResizedSize(in_params.padded_width, in_params.padded_height,
      currentScale, width, height);

    HOGHistogram::CountNumBlocks(width, height, hCellSizeX, hCellSizeY,
      hBlockSizeX, hBlockSizeY, num_blocks_x, num_blocks_y);

    HOGSVMSlider::CountWindows(num_blocks_x, num_blocks_y,
      hNumberOfBlockPerWindowX, hNumberOfBlockPerWindowY,
      map_width_array[i], map_height_array[i]);

    // go to the next scale
    currentScale *= scaleRatio;
  }
  m_adapter[in_idx].SetPyramidParameters(map_width_array, map_height_array);
}
